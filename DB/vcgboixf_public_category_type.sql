INSERT INTO public.category_type (category_type_id, category_type) VALUES (1, 'Tourism');
INSERT INTO public.category_type (category_type_id, category_type) VALUES (2, 'Food');
INSERT INTO public.category_type (category_type_id, category_type) VALUES (3, 'Portraits');
INSERT INTO public.category_type (category_type_id, category_type) VALUES (4, 'Emotions');
INSERT INTO public.category_type (category_type_id, category_type) VALUES (5, 'Nature');
INSERT INTO public.category_type (category_type_id, category_type) VALUES (6, 'Animals');
INSERT INTO public.category_type (category_type_id, category_type) VALUES (7, 'Shapes and colours');
INSERT INTO public.category_type (category_type_id, category_type) VALUES (8, 'Other');
