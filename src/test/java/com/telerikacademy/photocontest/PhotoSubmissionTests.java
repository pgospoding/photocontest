package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.telerikacademy.photocontest.repositories.contracts.ScoreRepository;
import com.telerikacademy.photocontest.services.PhotoSubmissionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.Helper.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class PhotoSubmissionTests {

    @Mock
    PhotoSubmissionRepository photoSubmissionRepository;

    @Mock
    ScoreRepository scoreRepository;

    @InjectMocks
    PhotoSubmissionServiceImpl photoSubmissionService;

    Contest contest;
    PhotoSubmission photoSubmission;

    @BeforeEach
    public void initialize() {
        contest = createMockContest();
        photoSubmission = createMockSubmission();
        photoSubmission.setContest(contest);
    }

    @Test
    public void create_should_throw_when_PhotoSubFromUserExistsInContest() {
        List<PhotoSubmission> list = new ArrayList<>();
        list.add(photoSubmission);
    Mockito.when(photoSubmissionRepository.getByContestIdandUser(photoSubmission.getUser().getUser_id(),
            photoSubmission.getContest().getContest_id())).thenReturn(list);
        assertThrows(DuplicateEntityException.class,()->photoSubmissionService.create(photoSubmission));
    }

    @Test
    public void create_should_callRepository_when_NoPhotoSubFromUserExistsInContest() {
        Mockito.when(photoSubmissionRepository.getByContestIdandUser(photoSubmission.getUser().getUser_id(),
                photoSubmission.getContest().getContest_id())).thenReturn(new ArrayList<>());
        photoSubmissionService.create(photoSubmission);
        Mockito.verify(photoSubmissionRepository,Mockito.times(1)).create(photoSubmission);
    }

    @Test
    public void getByID_should_callRepository(){
        photoSubmissionService.getByID(photoSubmission.getPhoto_submission_id());
        Mockito.verify(photoSubmissionRepository,Mockito.times(1)).getById(photoSubmission.getPhoto_submission_id());
    }

    @Test
    public void getPhotoSubmissionsByContestID_should_callRepository(){
        photoSubmissionService.getPhotoSubmissionsByContestId(photoSubmission.getContest().getContest_id());
        Mockito.verify(photoSubmissionRepository,Mockito.times(1)).getAllByContestID(photoSubmission.getContest().getContest_id());
    }

    @Test
    public void delete_should_throw_whenNotAdminOrAuthor(){
        User initiator = Helper.createMockActiveRegUser();
        initiator.setUser_id(28);
        initiator.setUsername("new");
        Mockito.when(photoSubmissionRepository.getById(photoSubmission.getPhoto_submission_id())).thenReturn(photoSubmission);
        Assertions.assertAll(
                () ->assertNotEquals(initiator.getUsername(),photoSubmission.getUser().getUsername()),
                () ->assertThrows(UnauthorizedOperationException.class,()->photoSubmissionService.delete(photoSubmission.getPhoto_submission_id(),initiator))
        );
    }



}
