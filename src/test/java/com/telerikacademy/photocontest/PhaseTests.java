package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestPhase;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.ContestPhaseRepository;
import com.telerikacademy.photocontest.repositories.contracts.ContestRepository;
import com.telerikacademy.photocontest.services.ContestPhaseServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.photocontest.Helper.*;

@ExtendWith(MockitoExtension.class)
public class PhaseTests {
    @Mock
    ContestPhaseRepository mockRepository;

    @Mock
    ContestRepository contestMockRepository;

    @InjectMocks
    ContestPhaseServiceImpl contestPhaseService;

    ContestPhase contestPhase;
    Contest contest;

    @BeforeEach
    public void initialize(){
        contestPhase = createMockContestPhase();
        contest = createMockContest();
        contestPhase.setContest(contest);
    }
    @Test
    public void getAllByPhase_should_return_ContestList() {
        List<Contest> contestList = new ArrayList<>();
        contestList.add(contest);

        Mockito.when(mockRepository.getAllByPhase(contestPhase.getPhaseType().getPhase_type_id())).thenReturn(contestList);

        List<Contest> returnedList = contestPhaseService.getAllByPhase(contestPhase.getPhaseType().getPhase_type_id());

        Assertions.assertEquals(returnedList.size(), contestList.size());
    }

    @Test
    public void getAll_should_callRepository() {
        contestPhaseService.getAll();
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByID_should_callRepository() {
        contestPhaseService.getById(contestPhase.getContest_phase_id());
        Mockito.verify(mockRepository, Mockito.times(1)).getById(contestPhase.getContest_phase_id());
    }

    @Test
    public void update_should_throw_when_initiatorIsRegular(){
        User initiator = createMockActiveRegUser();
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> contestPhaseService.update(contestPhase,contest,initiator));
    }

    @Test
    public void update_should_callRepository_when_initiatorIsContestOrganizer(){
        User initiator = createMockActiveOrganizer();
        contest.setOrganizer_id(initiator);
        contestPhaseService.update(contestPhase,contest,initiator);

        Mockito.verify(mockRepository,Mockito.times(1)).update(contestPhase);
    }

    @Test
    public void delete_should_throw_when_initiatorIsRegular(){
        User initiator = createMockActiveRegUser();
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> contestPhaseService.delete(contestPhase.getContest_phase_id(),contest,initiator));
    }

    @Test
    public void delete_should_callRepository_when_initiatorIsContestOrganizer(){
        User initiator = createMockActiveOrganizer();
        contest.setOrganizer_id(initiator);
        contestPhaseService.delete(contestPhase.getContest_phase_id(),contest,initiator);

        Mockito.verify(mockRepository,Mockito.times(1)).delete(contestPhase.getContest_phase_id());
    }

    @Test
    public void deleteByContestID_should_throw_when_initiatorIsRegular(){
        Mockito.when(contestMockRepository.getById(contest.getContest_id())).thenReturn(contest);
        User initiator = createMockActiveRegUser();
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> contestPhaseService.deleteByContestID(contest.getContest_id(),initiator));
    }

    @Test
    public void deleteByContestID_should_callRepository_when_initiatorIsContestOrganizer(){
        User initiator = createMockActiveOrganizer();
        contest.setOrganizer_id(initiator);
        List<ContestPhase> phaseList = new ArrayList<>();
        phaseList.add(contestPhase);

        Mockito.when(contestMockRepository.getById(contest.getContest_id())).thenReturn(contest);
        Mockito.when(mockRepository.getAllPhasesByContestID(contest.getContest_id())).thenReturn(phaseList);

        contestPhaseService.deleteByContestID(contest.getContest_id(),initiator);

        Mockito.verify(mockRepository,Mockito.times(1)).getAllPhasesByContestID(contest.getContest_id());
        Mockito.verify(mockRepository,Mockito.times(1)).delete(contestPhase);
    }

    @Test
    public void getAllPhasesByContestID_should_callRepository(){
        contestPhaseService.getAllPhasesByContestID(contest.getContest_id());
        Mockito.verify(mockRepository,Mockito.times(1)).getAllPhasesByContestID(contest.getContest_id());
    }

    @Test
    public void getPhaseOneByContestID_should_callRepository(){
        contestPhaseService.getPhaseOneByContestID(contest.getContest_id());
        Mockito.verify(mockRepository,Mockito.times(1)).getPhaseByPhaseNumberAndContestID(1,
                contest.getContest_id());
    }

    @Test
    public void getPhaseTwoByContestID_should_callRepository(){
        contestPhaseService.getPhaseTwoByContestID(contest.getContest_id());
        Mockito.verify(mockRepository,Mockito.times(1)).getPhaseByPhaseNumberAndContestID(2,
                contest.getContest_id());
    }

    @Test
    public void getPhaseThreeByContestID_should_callRepository(){
        contestPhaseService.getPhaseThreeByContestID(contest.getContest_id());
        Mockito.verify(mockRepository,Mockito.times(1)).getPhaseByPhaseNumberAndContestID(3,
                contest.getContest_id());
    }

    @Test
    public void getPhaseTypes_should_callRepository(){
        contestPhaseService.getPhaseTypes();
        Mockito.verify(mockRepository,Mockito.times(1)).getPhaseTypes();
    }

    @Test
    public void getContestPhase_should_callRepository(){
        contestPhaseService.getContestPhase(contest.getContest_id());
        Mockito.verify(mockRepository,Mockito.times(1)).getContestPhase(contest.getContest_id());
    }
}
