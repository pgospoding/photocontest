package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.repositories.contracts.CategoryRepository;
import com.telerikacademy.photocontest.services.CategoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.photocontest.Helper.createMockCategory;

@ExtendWith(MockitoExtension.class)
public class CategoryTests {

    @Mock
    CategoryRepository categoryRepository;

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    public void getAll_should_call_Repository(){
        Mockito.when(categoryRepository.getAll()).thenReturn(new ArrayList<CategoryType>());
        categoryService.getAll();

        Mockito.verify(categoryRepository,Mockito.times(1)).getAll();

    }
    @Test
    public void getById_should_returnMatchingCategory(){
        CategoryType categoryType = createMockCategory();
        Mockito.when(categoryRepository.getById(categoryType.getCategory_type_id())).thenReturn(categoryType);
        CategoryType categoryReturned = categoryService.getByID(categoryType.getCategory_type_id());

        Assertions.assertEquals(categoryType.getCategory_type(), categoryReturned.getCategory_type());

    }
}
