package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Comment;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.CommentRepository;
import com.telerikacademy.photocontest.services.CommentServiceImpl;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.photocontest.Helper.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CommentTests {

    @Mock
    CommentRepository commentRepository;

    @InjectMocks
    CommentServiceImpl commentService;

    Comment comment;

    @BeforeEach
    public void initialize() {
        comment = createMockComment();
    }

    @Test
    public void create_should_callRepository() {
        commentService.create(comment);
        //Assert
        Mockito.verify(commentRepository, Mockito.times(1)).create(comment);
    }

    @Test
    public void getById_should_returnExpectedComment() {
        Mockito.when(commentRepository.getById(comment.getComment_id())).thenReturn(comment);

        Comment returnedComment = commentService.getComment(comment.getComment_id());
        //Assert
        assertEquals(returnedComment, comment);
    }

    @Test
    public void update_should_throw_if_AuthorIsNotInitiator() {
        User initiator = createMockActiveAdmin();

        assertAll(
                () -> assertNotEquals(initiator, comment.getUser()),
                () -> assertThrows(UnauthorizedOperationException.class, () -> commentService.update(comment, initiator))
        );
    }

    @Test
    public void update_should_callRepository_when_InitiatorIsAuthor() {
        User initiator = comment.getUser();
        commentService.update(comment, initiator);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1)).update(comment);
    }

    @Test
    public void delete_should_throw_if_AuthorIsNotInitiator() {
        User initiator = createMockActiveAdmin();
        Mockito.when(commentRepository.getById(comment.getComment_id())).thenReturn(comment);
        assertAll(
                () -> assertNotEquals(initiator, comment.getUser()),
                () -> assertThrows(UnauthorizedOperationException.class, () -> commentService.delete(comment.getComment_id(), initiator))
        );
    }

    @Test
    public void delete_should_callRepository_when_InitiatorIsAuthor() {
        User initiator = comment.getUser();
        Mockito.when(commentRepository.getById(comment.getComment_id())).thenReturn(comment);
        commentService.delete(comment.getComment_id(), initiator);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1)).delete(comment.getComment_id());
    }

    @Test
    public void deleteByID_should_callRepository() {
        commentService.deleteByID(comment.getComment_id());
        //Assert
        Mockito.verify(commentRepository, Mockito.times(1)).delete(comment.getComment_id());
    }

    @Test
    public void deleteAllByContestID_should_callRepository() {
        commentService.deleteAllByContestID(comment.getPhotoSubmission().getContest().getContest_id());
        //Assert
        Mockito.verify(commentRepository, Mockito.times(1)).DeleteAllByContestID(comment.getPhotoSubmission()
                .getContest().getContest_id());
    }

    @Test
    public void getBySubmissionID_should_callRepository() {
        commentService.getBySubmissionID(comment.getPhotoSubmission().getPhoto_submission_id());

        Mockito.verify(commentRepository, Mockito.times(1)).getbySubmissionID(comment.getPhotoSubmission().getPhoto_submission_id());
    }

}
