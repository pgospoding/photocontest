package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.DuplicateUsernameException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestWithPhase;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.ContestPhaseRepository;
import com.telerikacademy.photocontest.repositories.contracts.ContestRepository;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.telerikacademy.photocontest.repositories.contracts.UserRepository;
import com.telerikacademy.photocontest.services.ContestServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.telerikacademy.photocontest.Helper.*;

@ExtendWith(MockitoExtension.class)
public class ContestTests {

    @Mock
    ContestRepository contestMockRepository;

    @Mock
    ContestPhaseRepository contestPhaseMockRepository;

    @Mock
    PhotoSubmissionRepository photoSubmissionMockRepository;

    @Mock
    UserRepository userMockRepository;

    @InjectMocks
    ContestServiceImpl contestService;

    @Test
    public void getAll_should_callRepository_when_optionalsEmpty() {
        contestService.getAll(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());

        Mockito.verify(contestMockRepository, Mockito.times(1)).filter(Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty());
    }

    @Test
    public void getAll_should_callRepository_when_filterPresent() {
        contestService.getAll(Optional.ofNullable("title"), Optional.empty(), Optional.ofNullable(1), Optional.empty());

        Mockito.verify(contestMockRepository, Mockito.times(1)).filter(Optional.ofNullable("title"), Optional.empty(),
                Optional.ofNullable(1), Optional.empty());
    }

    @Test
    public void getAll_should_returnList_when_matchExists() {
        List<Contest> list = new ArrayList<>();
        list.add(createMockContest());
        Mockito.when(contestMockRepository.filter(Optional.ofNullable("Let's"), Optional.empty(),
                Optional.empty(), Optional.empty())).thenReturn(list);
        Assertions.assertTrue(contestService.getAll(Optional.ofNullable("Let's"), Optional.empty(),
                Optional.empty(), Optional.empty()).size() == 1);
    }

    @Test
    public void getAllWithPhases_should_callRepository_when_optionalsEmpty() {
        contestService.getAllWithPhases(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());

        Mockito.verify(contestMockRepository, Mockito.times(1)).filterWithPhases(Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty());
    }

    @Test
    public void getAllWithPhases_should_callRepository_when_filterPresent() {
        contestService.getAllWithPhases(Optional.ofNullable("title"), Optional.empty(), Optional.ofNullable(1), Optional.empty());

        Mockito.verify(contestMockRepository, Mockito.times(1)).filterWithPhases(Optional.ofNullable("title"), Optional.empty(),
                Optional.ofNullable(1), Optional.empty());
    }

    @Test
    public void getAllWith_should_returnList_when_matchExists() {
        List<ContestWithPhase> list = new ArrayList<>();
        list.add(createMockContestWithPhase());
        Mockito.when(contestMockRepository.filterWithPhases(Optional.ofNullable("Let's"), Optional.empty(),
                Optional.empty(), Optional.empty())).thenReturn(list);
        Assertions.assertTrue(contestService.getAllWithPhases(Optional.ofNullable("Let's"), Optional.empty(),
                Optional.empty(), Optional.empty()).size() == 1);
    }

    @Test
    public void getById_should_returnContest() {
        Contest contest = Helper.createMockContest();
        int id = contest.getContest_id();
        Mockito.when(contestMockRepository.getById(id)).thenReturn(contest);

        Contest returned = contestService.getById(id);

        Assertions.assertEquals(contest, returned);
    }

    @Test
    public void getByOrganizer_should_callRepository() {
        User user = createMockActiveOrganizer();
        contestService.getByOrganizer(user);
        Mockito.verify(contestMockRepository, Mockito.times(1))
                .getByOrganizer(user);
    }

    @Test
    public void getByPhaseId_should_callRepository() {
       contestService.getByPhaseId(1);
        Mockito.verify(contestPhaseMockRepository, Mockito.times(1))
                .getContestsByPhase(1);
    }

    @Test
    public void create_should_callRepositoryCreateMethod_when_TitleIsUnique() {
        Contest mockContest = createMockContest();
        Mockito.when(contestMockRepository.getByTitle(mockContest.getTitle()))
                .thenThrow(ObjectNotFoundException.class);

        //Act
        contestService.create(mockContest);

        //Assert
        Mockito.verify(contestMockRepository, Mockito.times(1)).create(mockContest);
    }

    @Test
    public void create_should_throw_when_sameTitleFound() {
        String existingTitle = "existingTitle";
        Contest mockExistingContest = Helper.createMockContest();
        Contest mockNewContest = Helper.createMockContest();
        mockExistingContest.setTitle(existingTitle);
        mockNewContest.setTitle(existingTitle);
        Mockito.when(contestMockRepository.getByTitle(existingTitle)).thenReturn(mockExistingContest);

        Assertions.assertThrows(DuplicateEntityException.class, () -> contestService.create(mockNewContest));

    }

    @Test
    public void update_should_throwException_when_userIsNotOrganizerOrAdmin() {
        // Arrange
        Contest mockContest = createMockContest();
        User mockInitiator = createMockActiveRegUser();
        User mockCreator = new User();
        mockCreator.setUsername("MockUser2");
        mockContest.setOrganizer_id(mockCreator);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.update(mockContest, mockInitiator));
    }
    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingContest() {
        // Arrange
        Contest mockContest = createMockContest();

        // Act
        contestService.update(mockContest, mockContest.getOrganizer_id());

        // Assert
        Mockito.verify(contestMockRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    void delete_should_throwException_when_initiatorIsNotAdminOrOrganizer() {
        // Arrange
        Contest mockContest = createMockContest();
        User mockInitiator = createMockActiveRegUser();
        mockInitiator.setUsername("MockInitiator");

        Mockito.when(contestMockRepository.getById(mockContest.getContest_id()))
                .thenReturn(mockContest);

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.delete(mockContest.getContest_id(), mockInitiator));
    }
    @Test
    void delete_should_callRepository_when_initiatorIsNotAdminAndIsOrganizer() {
        // Arrange
        Contest mockContest = createMockContest();

        Mockito.when(contestMockRepository.getById(mockContest.getContest_id()))
                .thenReturn(mockContest);

        // Act
        contestService.delete(mockContest.getContest_id(), mockContest.getOrganizer_id());

        // Assert
        Mockito.verify(contestMockRepository, Mockito.times(1))
                .delete(mockContest.getContest_id());
    }

    @Test
    void addJurors_should_callRepository_when_initiatorIsNotAdminAndIsOrganizer() {
        // Arrange
        Contest mockContest = createMockContest();
        User userToAdd = createMockActiveRegUser();
        User logged = createMockActiveOrganizer();

        Mockito.when(contestMockRepository.getById(mockContest.getContest_id()))
                .thenReturn(mockContest);

        // Act
        contestService.addJurors(mockContest.getContest_id(),userToAdd, logged);

        // Assert
        Mockito.verify(contestMockRepository, Mockito.times(1))
                .update(mockContest);
    }
    @Test
    void addJurors_should_throwException_when_initiatorIsNotAdminAndIsOrganizer() {
        // Arrange
        Contest mockContest = createMockContest();
        User userToAdd = createMockActiveRegUser();

        Mockito.when(contestMockRepository.getById(mockContest.getContest_id()))
                .thenReturn(mockContest);

        // Act
        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.addJurors(mockContest.getContest_id(),userToAdd, mockContest.getOrganizer_id()));
    }

    @Test
    void removeJurors_should_callRepository_when_initiatorIsNotAdminAndIsOrganizer() {
        // Arrange
        Contest mockContest = createMockContest();
        User userToAdd = createMockActiveRegUser();
        User logged = createMockActiveOrganizer();

        Mockito.when(contestMockRepository.getById(mockContest.getContest_id()))
                .thenReturn(mockContest);

        // Act
        contestService.removeJurors(mockContest.getContest_id(),userToAdd, logged);

        // Assert
        Mockito.verify(contestMockRepository, Mockito.times(1))
                .update(mockContest);
    }
    @Test
    void removeJurors_should_throwException_when_initiatorIsNotAdminAndIsOrganizer() {
        // Arrange
        Contest mockContest = createMockContest();
        User userToAdd = createMockActiveRegUser();

        Mockito.when(contestMockRepository.getById(mockContest.getContest_id()))
                .thenReturn(mockContest);

        // Act
        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.removeJurors(mockContest.getContest_id(),userToAdd, mockContest.getOrganizer_id()));
    }
    @Test
    void addParticipants_should_callRepository_when_initiatorIsNotAdminAndIsOrganizer() {
        // Arrange
        Contest mockContest = createMockContest();
        User userToAdd = createMockActiveRegUser();
        User logged = createMockActiveOrganizer();

        Mockito.when(contestMockRepository.getById(mockContest.getContest_id()))
                .thenReturn(mockContest);

        // Act
        contestService.addParticipants(mockContest.getContest_id(),userToAdd);

        // Assert
        Mockito.verify(contestMockRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void getContestOrganizers_should_callRepository(){
        contestService.getContestOrganizers();
        Mockito.verify(contestMockRepository,Mockito.times(1))
                .getContestOrganizers();
    }

//    TODO finalize
//    public void updateFinishedContests_should_callRepositories(){
//        List<Integer> contestIds = new ArrayList<>();
//        contestIds.add(1);
//        contestIds.add(2);
//        Mockito.when(contestPhaseMockRepository.getAllToBeMarkedAsFinished()).thenReturn(contestIds);
//
//    }

    @Test
    public void calculateWinners_should_throwException_when_firstPlaceNotEmpty(){
        Contest contest = createMockContest();
        Set<PhotoSubmission> firstPlace = new HashSet<>();
        firstPlace.add(createMockSubmission());
        contest.setFirstPlaceWinners(firstPlace);
        Mockito.when(contestMockRepository.getById(contest.getContest_id())).thenReturn(contest);
        Mockito.when(photoSubmissionMockRepository.getTop10PhotoSubmissionsByContestID(contest.getContest_id()))
                .thenReturn(new ArrayList<PhotoSubmission>());

        Assertions.assertThrows(UnauthorizedOperationException.class, ()-> contestService.calculateWinners(contest.getContest_id()));
    }

    @Test
    public void calculateWinners_should_callUpdate_when_firstPlaceWasEmpty(){
        Contest contest = createMockContest();
        List<PhotoSubmission> candidates = new ArrayList<PhotoSubmission>();
        PhotoSubmission photoSubmission = createMockSubmission();
        photoSubmission.setTotalScore(10);
        candidates.add(photoSubmission);
        Mockito.when(contestMockRepository.getById(contest.getContest_id())).thenReturn(contest);
        Mockito.when(photoSubmissionMockRepository.getTop10PhotoSubmissionsByContestID(contest.getContest_id()))
                .thenReturn(candidates);

        contestService.calculateWinners(contest.getContest_id());
        Mockito.verify(contestMockRepository,Mockito.times(1)).update(contest);
    }

    @Test
    public void calculateWinners_should_addToLists_when_firstPlaceWasEmpty(){
        Contest contest = createMockContest();
        List<PhotoSubmission> candidates = new ArrayList<PhotoSubmission>();
        PhotoSubmission photoSubmission1 = createMockSubmission();
        photoSubmission1.setTotalScore(10);
        candidates.add(photoSubmission1);
        PhotoSubmission photoSubmission2 = createMockSubmission();
        photoSubmission2.setPhoto_submission_id(2);
        photoSubmission2.setTotalScore(8);
        candidates.add(photoSubmission2);
        PhotoSubmission photoSubmission3 = createMockSubmission();
        photoSubmission3.setPhoto_submission_id(3);
        photoSubmission3.setTotalScore(7);
        candidates.add(photoSubmission3);

        Mockito.when(contestMockRepository.getById(contest.getContest_id())).thenReturn(contest);
        Mockito.when(photoSubmissionMockRepository.getTop10PhotoSubmissionsByContestID(contest.getContest_id()))
                .thenReturn(candidates);

        contestService.calculateWinners(contest.getContest_id());
        Assertions.assertAll(
                () -> Assertions.assertTrue(contest.getFirstPlaceWinners().contains(photoSubmission1)),
                () -> Assertions.assertTrue(contest.getSecondPlaceWinners().contains(photoSubmission2)),
                () -> Assertions.assertTrue(contest.getThirdPlaceWinners().contains(photoSubmission3))
        );
    }

    @Test
    public void awardPointsForParticipation_should_addAPointToAllParticipants(){
        Contest contest = createMockContest();
        User participant1 = createMockActiveRegUser();
        int points1 = participant1.getTotalPoints()+1;
        User participant2 = createMockActiveRegUser();
        participant2.setUser_id(11);
        participant2.setUsername("newnew");
        int points2 = participant2.getTotalPoints()+1;
        contest.getParticipants().add(participant1);
        contest.getParticipants().add(participant2);

        contestService.awardPointsForParticipation(contest);

        Assertions.assertAll(
                () -> Assertions.assertEquals(points1, participant1.getTotalPoints()),
                () -> Assertions.assertEquals(points2, participant2.getTotalPoints()));

    }



}


