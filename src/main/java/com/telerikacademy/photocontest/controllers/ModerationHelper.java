package com.telerikacademy.photocontest.controllers;

import com.telerikacademy.photocontest.services.contracts.UserService;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ModerationHelper {
    public static final String AUTHORIZATION_HTTP_HEADER = "Authorization";
    public UserService service;

    @Autowired
    public ModerationHelper(UserService service) {
        this.service = service;
    }

    public static String moderateText(String url){
        String image_url = url;
        String api_url = String.format("https://api.moderatecontent.com/moderate/?key=8c80a1e1e52292b4669cbe68ab330350&url=%s",image_url);
        Request request_test = new Request.Builder().url(api_url).build();
        Response response = null;
        try {
            response = new OkHttpClient().newBuilder().build().newCall(request_test).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String string_response =  response.body().string();
            JSONObject text_json = new JSONObject(string_response);
            String rating_index = String.valueOf(text_json.getInt("rating_index"));
            return rating_index;
        } catch (IOException e) {
            e.printStackTrace();
        }
        String return_test = "failed";
        return return_test;
    }

}
