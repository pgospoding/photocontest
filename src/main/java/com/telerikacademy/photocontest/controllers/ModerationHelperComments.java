package com.telerikacademy.photocontest.controllers;

import com.telerikacademy.photocontest.services.contracts.UserService;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ModerationHelperComments {
    public static final String AUTHORIZATION_HTTP_HEADER = "Authorization";
    public UserService service;

    @Autowired
    public ModerationHelperComments(UserService service) {
        this.service = service;
    }

    public static String moderateText(String check_string){
        String http_test=String.join(" ",check_string);
        MediaType mediaType = MediaType.parse("text/plain");
        okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, http_test);
        Request request_test = new Request.Builder().url("https://api.apilayer.com/bad_words?censor_character=").addHeader("apikey", "6YqqABFMCJ3HL8voxQNp8NjNJ67yAQC1").method("POST", body).build();
        Response response = null;
        try {
            response = new OkHttpClient().newBuilder().build().newCall(request_test).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String string_response =  response.body().string();
            JSONObject text_json = new JSONObject(string_response);
            String text_censored = text_json.getString("censored_content");
            return text_censored;
        } catch (IOException e) {
            e.printStackTrace();
        }
        String return_test = "failed";
        return return_test;
    }

}
