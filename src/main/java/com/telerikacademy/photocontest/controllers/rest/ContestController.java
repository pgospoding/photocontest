package com.telerikacademy.photocontest.controllers.rest;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.ImageUploadHelper;
import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.DTOs.CommentDTO;
import com.telerikacademy.photocontest.models.DTOs.CommentUpdateDTO;
import com.telerikacademy.photocontest.models.DTOs.ContestDTO;
import com.telerikacademy.photocontest.models.DTOs.PhotoSubmissionDTO;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;
import com.telerikacademy.photocontest.services.contracts.CommentService;
import com.telerikacademy.photocontest.services.contracts.ContestService;
import com.telerikacademy.photocontest.services.contracts.PhotoSubmissionService;
import com.telerikacademy.photocontest.services.contracts.UserService;
import com.telerikacademy.photocontest.utils.mappers.CommentMapper;
import com.telerikacademy.photocontest.utils.mappers.CommentUpdateMapper;
import com.telerikacademy.photocontest.utils.mappers.ContestMapper;
import com.telerikacademy.photocontest.utils.mappers.PhotoSubmissionMapper;
import com.telerikacademy.photocontest.models.DTOs.*;
import com.telerikacademy.photocontest.services.contracts.*;
import com.telerikacademy.photocontest.utils.mappers.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests")
public class ContestController {
    private final ContestService contestService;
    private final ContestMapper mapper;
    private final CommentMapper commentMapper;
    private final CommentUpdateMapper commentUpdateMapper;
    private final PhotoSubmissionService photoSubmissionService;
    private final CommentService commentService;
    private final ContestPhaseService contestPhaseService;
    private final PhotoSubmissionMapper photoSubmissionMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ScoreService scoreService;
    private final PhaseTypeService phaseTypeService;
    private final ScoreMapper scoreMapper;
    private final PhaseMapper phaseMapper;

    public ContestController(ContestService service,
                             ContestMapper mapper,
                             CommentMapper commentMapper,
                             CommentUpdateMapper commentUpdateMapper,
                             PhotoSubmissionService photoSubmissionService,
                             CommentService commentService,
                             ContestPhaseService contestPhaseService,
                             ContestService contestService, PhotoSubmissionMapper photoSubmissionMapper,
                             AuthenticationHelper authenticationHelper,
                             UserService userService,
                             ScoreService scoreService,
                             PhaseTypeService phaseTypeService, ScoreMapper scoreMapper,
                             PhaseMapper phaseMapper) {
        this.contestService = service;
        this.mapper = mapper;
        this.commentMapper = commentMapper;
        this.commentUpdateMapper = commentUpdateMapper;
        this.photoSubmissionService = photoSubmissionService;
        this.commentService = commentService;
        this.contestPhaseService = contestPhaseService;
        this.photoSubmissionMapper = photoSubmissionMapper;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.scoreService = scoreService;
        this.phaseTypeService = phaseTypeService;
        this.scoreMapper = scoreMapper;
        this.phaseMapper = phaseMapper;
    }


    @GetMapping
    public List<Contest> getAll(@RequestParam(value = "phase_id",required = false) Integer phaseId,
                                         @RequestParam(value = "title",required = false) String title,
                                         @RequestParam(value = "category",required = false) Integer categoryId,
                                         @RequestParam(value = "sort_by", required = false) String sort) {
        return contestService.getAll(Optional.ofNullable(title),
                Optional.ofNullable(categoryId),
                Optional.ofNullable(phaseId),
                Optional.ofNullable(ContestSortOptions.valueOfPreview(sort)));
    }
    @GetMapping("/withphases")
    public List<ContestWithPhase> getAllWithPhases(@RequestParam(value = "phase_id",required = false) Integer phaseId,
                                @RequestParam(value = "title",required = false) String title,
                                @RequestParam(value = "category",required = false) Integer categoryId,
                                @RequestParam(value = "sort_by", required = false) String sort) {
        return contestService.getAllWithPhases(Optional.ofNullable(title),
                Optional.ofNullable(categoryId),
                Optional.ofNullable(phaseId),
                Optional.ofNullable(ContestSortOptions.valueOfPreview(sort)));
    }

    @GetMapping("/{id}")
    public Contest getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
/*
            User loggedUser = authenticationHelper.tryGetUser(headers);
*/
            return contestService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping
    public Contest create(@RequestHeader HttpHeaders headers,
                          @Valid @RequestBody ContestDTO contestDTO) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Contest contest = mapper.dtoToObject(contestDTO, loggedUser);
            contestService.create(contest);
            return contest;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }


    }

    @PutMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers,
                          @Valid @RequestBody ContestDTO contestDTO, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Contest contest = mapper.dtoToObject(contestDTO, id);
            contestService.update(contest, loggedUser);
            return contest;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            contestService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/submission/photo")
    public void create(@RequestHeader HttpHeaders headers, @RequestParam MultipartFile multipartFile,
                       @RequestParam String title, @RequestParam String story, @RequestParam Integer contest_id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Image image2 = ImageUploadHelper.uploadCloudinary(multipartFile, loggedUser);
            PhotoSubmissionDTO photoSubmissionDTO = new PhotoSubmissionDTO();
            photoSubmissionDTO.setTitle(title);
            photoSubmissionDTO.setStory(story);
            photoSubmissionDTO.setContest_id(contest_id);
            PhotoSubmission photoSubmission = photoSubmissionMapper.dtoToObject(photoSubmissionDTO, loggedUser, image2);
            photoSubmissionService.create(photoSubmission);
            contestService.addParticipants(contest_id, loggedUser);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/photosubmission/delete/{id}")
    public void deletePhotoSubmission(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            photoSubmissionService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/submission/{id}")
    public PhotoSubmission getSubmissionById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            return photoSubmissionService.getByID(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/contest/{id}/submissions")
    public List<PhotoSubmission> getContestSubmissionsById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            return photoSubmissionService.getPhotoSubmissionsByContestId(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/submission/{id}/comment")
    public Comment createComment(@RequestHeader HttpHeaders headers,
                                 @Valid @RequestBody CommentDTO commentDTO, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Comment comment = commentMapper.dtoToObject(commentDTO, loggedUser);
            return commentService.create(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @DeleteMapping("/comment/delete/{id}")
    public void deleteComment(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            commentService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/comments/{comment_id}")
    public Comment updateComment(@RequestHeader HttpHeaders headers, @Valid @RequestBody CommentUpdateDTO commentUpdateDTO, @PathVariable int comment_id) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(headers);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You need to log in");
        }
        Comment comment = commentUpdateMapper.dtoToObject(commentUpdateDTO, comment_id);
        try {
            return commentService.update(comment, initiator);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ObjectNotFoundException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not the author of this item");
        }
    }

    @PutMapping("/{id}/jurors/{userId}")
    public Contest addJurors(@PathVariable int id, @PathVariable int userId, @RequestHeader HttpHeaders headers) {
        //Authentication helper throws ResponseStatusException directly
        User loggedUser = authenticationHelper.tryGetUser(headers);

        try {
            User user = userService.getById(userId, loggedUser);
            return contestService.addJurors(id, user, loggedUser);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/submission/{id}/score")
    public Score createScore(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody ScoreDTO scoreDTO, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            PhotoSubmission photoSubmission = photoSubmissionService.getByID(id);
            Score score = scoreMapper.dtoToObject(scoreDTO, loggedUser, photoSubmission);
            return scoreService.create(score);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/submission/delete/{id}")
    public void deleteScore(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            scoreService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("{id}/scores/{score_id}")
    public Score updateScore(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody ScoreDTO scoreDTO,
                             @PathVariable int score_id, @PathVariable int id) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(headers);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You need to log in");
        }
        PhotoSubmission photoSubmission = photoSubmissionService.getByID(id);
        Score score = scoreMapper.dtoToObject(scoreDTO, initiator, photoSubmission);
        try {
            return scoreService.update(score, initiator);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ObjectNotFoundException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not the author of this item");
        }
    }

    @GetMapping("/scores")
    public List<Score> getAll() {

        return scoreService.getAll();
    }

    @GetMapping("scores/{score_id}")
    public Score getByIdScore(@RequestHeader HttpHeaders headers, @PathVariable int score_id) {
        try {
/*
            User loggedUser = authenticationHelper.tryGetUser(headers);
*/
            return scoreService.getById(score_id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/comment/{id}")
    public Comment getCommentById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            return commentService.getComment(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/phase/{id}")
    public List<Contest> getAllByPhase(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            return contestService.getByPhaseId(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/searchbyphototitle")
    public List<PhotoSubmission> searchByTitle(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> title) {
        try {
            authenticationHelper.tryGetUser(headers);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You need to log in");
        }
        return photoSubmissionService.searchByTitle(title);
    }

    @PostMapping("/{id}/phases")
    public ContestPhase create(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody PhaseDTO phaseDTO, @PathVariable int id
    ) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(id);
            ContestPhase phase = phaseMapper.dtoToObject(phaseDTO, contest);
            contestPhaseService.create(phase);
            return phase;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }


    }

    @PutMapping("/{id}/phases/{phase_id}")
    public ContestPhase update(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody PhaseDTO phaseDTO,
                               @PathVariable int id,
                               @PathVariable int phase_id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(id);
            ContestPhase contestPhase = phaseMapper.dtoToObject(phaseDTO, phase_id, contest);
            contestPhaseService.update(contestPhase, contest, loggedUser);
            return contestPhase;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/phases/{phase_id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id, @PathVariable int phase_id) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(id);
            contestPhaseService.delete(phase_id, contest, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/running/participants/{id}")
    public List getRunningContestsByParticipant(@PathVariable int id){
        return contestService.getRunningContestsByParticipant(id);
    }

    @GetMapping("/finished/participants/{id}")
    public List getFinishedContestsByParticipant(@PathVariable int id){
        return contestService.getFinishedContestsByParticipant(id);
    }


}
