package com.telerikacademy.photocontest.controllers.rest;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.DTOs.SelfUpdDTO;
import com.telerikacademy.photocontest.models.DTOs.UserCreateDTO;
import com.telerikacademy.photocontest.models.DTOs.UserDTO_byIdUpd;
import com.telerikacademy.photocontest.services.contracts.UserService;
import com.telerikacademy.photocontest.utils.mappers.UserByIdUpdMapper;
import com.telerikacademy.photocontest.utils.mappers.UserCreateMapper;
import com.telerikacademy.photocontest.utils.mappers.SelfUpdateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    private final AuthenticationHelper authenticationHelper;
    private final SelfUpdateMapper selfUpdateMapper;
    private final UserByIdUpdMapper userByIdUpdMapper;
    private final UserCreateMapper userCreateMapper;


    @Autowired
    public UserController(UserService userService,
                         AuthenticationHelper authenticationHelper,
                          SelfUpdateMapper selfUpdateMapper,
                          UserByIdUpdMapper userByIdUpdMapper,
                          UserCreateMapper userCreateMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.selfUpdateMapper = selfUpdateMapper;
        this.userByIdUpdMapper = userByIdUpdMapper;
        this.userCreateMapper = userCreateMapper;
    }

//Tested in postman
    @GetMapping
    public List<User> getUsers(
            @RequestParam(required = false) Optional<String> search,
            @RequestHeader HttpHeaders headers) {
        //AuthenticationHelper throws ResponseStatusExceptions
        User initiator = authenticationHelper.tryGetUser(headers);

        try {
            return userService.getAll(search, initiator);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    //To view user information by id, users should be at least organizers.
    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        //AuthenticationHelper throws ResponseStatusExceptions
        User initiator = authenticationHelper.tryGetUser(headers);
        try {
            return userService.getById(id,initiator);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    Tested in Postman, attention: needs passwordConfirmation field in JSON
    @PostMapping
    public User createUser(@Valid @RequestBody UserCreateDTO userCreateDTO) {
        User user = userCreateMapper.dtoToObject(userCreateDTO);
        try {
            return userService.create(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    //Change regular user info by id, block, unblock, give and take admin rights - by admin only
    @PutMapping("/{id}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @Valid @RequestBody UserDTO_byIdUpd userDTO,
                           @PathVariable int id) {
        User initiator;
        //AuthenticationHelper throws ResponseStatusExceptions
        initiator = authenticationHelper.tryGetUser(headers);
        User user;
        try {
            user = userByIdUpdMapper.dtoToObject(userDTO, id);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with id %d does not exist", id));
        }
        try {
            return userService.updateById(initiator, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }  catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("User with email %s already exists", user.getEmail()));
        }
    }


    //Tested
    //Change everything except id, username, role and status - by regular user himself
    @PutMapping("/me")
    public User updateUser(@RequestHeader HttpHeaders headers, @Valid @RequestBody SelfUpdDTO
            userDTOOwnInfoUpd) {
        User userWhoInitiatedAction;
        User user;
        try {
            userWhoInitiatedAction = authenticationHelper.tryGetUser(headers);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You need to log in to update your own information");
        }
        user = selfUpdateMapper.dtoToObject(userDTOOwnInfoUpd, userWhoInitiatedAction);
        try {
            return userService.updateSelf(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    //Tested in Postman
    @DeleteMapping("/{id}")
    public String deleteUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User userWhoInitiatesAction;
        try {
            userWhoInitiatesAction = authenticationHelper.tryGetUser(headers);
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You need to log in to delete users");
        }
        try {
            return userService.delete(id, userWhoInitiatesAction);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with id %d does not exist", id));
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You don't have permission to delete users.");
        }
    }

    @GetMapping("/stats")
    public long getNumberOfUsers() {
        return userService.getNumberOfUsers();
    }
}
