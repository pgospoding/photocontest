package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.models.DTOs.LoginDTO;
import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.exceptions.AuthenticationFailureException;
import com.telerikacademy.photocontest.exceptions.DuplicateEmailException;
import com.telerikacademy.photocontest.exceptions.DuplicateUsernameException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.DTOs.UserCreateDTO;
import com.telerikacademy.photocontest.models.Image;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.services.contracts.ImageService;
import com.telerikacademy.photocontest.services.contracts.UserService;
import com.telerikacademy.photocontest.utils.mappers.UserCreateMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
//TODO correct to login if we learn how to avoid relative linking
@RequestMapping("/")
public class AuthenticationMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserCreateMapper userCreateMapper;
    private final ImageService imageService;

    public AuthenticationMvcController(AuthenticationHelper authenticationHelper, UserService userService, UserCreateMapper userCreateMapper, ImageService imageService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userCreateMapper = userCreateMapper;
        this.imageService = imageService;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDTO", new LoginDTO());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDTO") LoginDTO loginDTO, BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        try {
            authenticationHelper.verifyAuthentication(loginDTO.getUsername(), loginDTO.getPassword());
            session.setAttribute("currentUser", loginDTO.getUsername());
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "authentication_error", e.getMessage());
            return "login";
        } catch (ObjectNotFoundException e) {
            bindingResult.rejectValue("username", "authentication_error", e.getMessage());
            return "login";
        }
        User user = userService.getByUsername(loginDTO.getUsername());
        int id = user.getUser_id();
        try {
            Image image = imageService.getImageById(user.getPhoto().getImage_id());
            session.setAttribute("pictureLink", image.getImage_link());
        } finally {

            if (userService.getByUsername(loginDTO.getUsername()).isActiveAdmin()) {
                String adminStatus = "admin";
                session.setAttribute("admin", adminStatus);
            }

            if (userService.getByUsername(loginDTO.getUsername()).getRoleType().getRole().equals("Organizer")) {
                String organizerStatus = "organizer";
                session.setAttribute("organizer", organizerStatus);
            }
            session.setAttribute("user_id",user.getUser_id());
            return "redirect:/";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("currentUser");
        session.removeAttribute("admin");
        session.removeAttribute("pictureLink");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userCreateDTO", new UserCreateDTO());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("userCreateDTO") UserCreateDTO userCreateDTO,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        if (!userCreateDTO.getPasswordConfirmation().equals(userCreateDTO.getPassword())) {
            bindingResult.rejectValue("password", "passwords_dont_match", "Passwords don't match");
            return "register";
        }
        User newUser;
        try {
            newUser = userCreateMapper.dtoToObject(userCreateDTO);
            userService.create(newUser);
            return "redirect:/login";
        } catch (DuplicateUsernameException e) {
            bindingResult.rejectValue("username", "duplicate_username", e.getMessage());
            return "register";
        } catch (DuplicateEmailException e) {
            bindingResult.rejectValue("email", "duplicate_email", e.getMessage());
            return "register";
        }
    }
}
