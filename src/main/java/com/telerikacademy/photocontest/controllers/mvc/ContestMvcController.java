package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.ImageUploadHelper;
import com.telerikacademy.photocontest.controllers.ModerationHelper;
import com.telerikacademy.photocontest.controllers.ModerationHelperComments;
import com.telerikacademy.photocontest.exceptions.AuthenticationFailureException;
import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.DTOs.CommentDTO;
import com.telerikacademy.photocontest.models.DTOs.ContestDTO;
import com.telerikacademy.photocontest.models.DTOs.PhotoSubmissionDTO;
import com.telerikacademy.photocontest.models.DTOs.ScoreDTO;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;
import com.telerikacademy.photocontest.services.ScoreServiceImpl;
import com.telerikacademy.photocontest.services.contracts.*;
import com.telerikacademy.photocontest.utils.mappers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import static java.lang.String.valueOf;

@Controller
@RequestMapping("/")
public class ContestMvcController {

    private final ImageService imageService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserCreateMapper userCreateMapper;
    private final UserByIdUpdMapper userByIdUpdMapper;
    private final ContestMapper contestMapper;
    private final SelfUpdateMapper selfUpdateMapper;
    private final MVCUserByIdUpdMapper mVCUserByIdUpdMapper;
    private final RoleTypeService roleTypeService;
    private final StatusService statusService;
    private final CategoryService categoryService;
    private final ContestService contestService;
    private final ContestPhaseService contestPhaseService;
    private final PhaseTypeService phaseTypeService;
    private final PhotoSubmissionService photoSubmissionService;
    private final PhotoSubmissionMapper photoSubmissionMapper;
    private final CommentService commentService;
    private final ScoreServiceImpl scoreService;
    private final ScoreDTO scoreDTO;
    private final ScoreMapper scoreMapper;
    private final CommentDTO commentDTO;
    private final CommentMapper commentMapper;
    private final ModerationHelper moderationHelper;
    private final ModerationHelperComments moderationHelperComments;

    @Autowired
    public ContestMvcController(ImageService imageService,
                                UserService userService,
                                AuthenticationHelper authenticationHelper,
                                UserCreateMapper userCreateMapper,
                                UserByIdUpdMapper userByIdUpdMapper,
                                ContestMapper contestMapper, SelfUpdateMapper selfUpdateMapper,
                                MVCUserByIdUpdMapper mVCUserByIdUpdMapper, RoleTypeService roleTypeService, StatusService statusService, CategoryService categoryService, ContestService contestService, ContestPhaseService contestPhaseService, PhaseTypeService phaseTypeService, PhotoSubmissionService photoSubmissionService, PhotoSubmissionMapper photoSubmissionMapper, CommentService commentService, ScoreServiceImpl scoreService, ScoreDTO scoreDTO, ScoreMapper scoreMapper, CommentDTO commentDTO, CommentMapper commentMapper, ModerationHelper moderationHelper, ModerationHelperComments moderationHelperComments) {
        this.imageService = imageService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userCreateMapper = userCreateMapper;
        this.userByIdUpdMapper = userByIdUpdMapper;
        this.contestMapper = contestMapper;
        this.selfUpdateMapper = selfUpdateMapper;
        this.mVCUserByIdUpdMapper = mVCUserByIdUpdMapper;
        this.roleTypeService = roleTypeService;
        this.statusService = statusService;
        this.categoryService = categoryService;
        this.contestService = contestService;
        this.contestPhaseService = contestPhaseService;
        this.phaseTypeService = phaseTypeService;
        this.photoSubmissionService = photoSubmissionService;
        this.photoSubmissionMapper = photoSubmissionMapper;
        this.commentService = commentService;
        this.scoreService = scoreService;
        this.scoreDTO = scoreDTO;
        this.scoreMapper = scoreMapper;
        this.commentDTO = commentDTO;
        this.commentMapper = commentMapper;
        this.moderationHelper = moderationHelper;
        this.moderationHelperComments = moderationHelperComments;
    }

    @ModelAttribute("categories")
    public List<CategoryType> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("phases")
    public List<PhaseType> populatePhases() {
        return phaseTypeService.getPhaseTypes();
    }


    @GetMapping("/new")
    public String showNewContestForm(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }

        return "contest-new";
    }


    //TODO refactor create phase 1, phase 2 and phase 3 with helper method.
    @PostMapping("/new")
    public String createContest(Model model, HttpSession session,
                                @RequestParam(value = "phase1_start_time") String phase1StartDate,
                                @RequestParam(value = "phase1_end_time") String phase1EndDate,
                                @RequestParam(value = "phase2_end_time") String phase2EndDate,
                                @RequestParam(value = "title") String contest_title,
                                @RequestParam(value = "contest_category") int contest_category,
                                @RequestParam("image") MultipartFile multipartFile) throws IOException, ParseException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        Image image;
        try {
            image = ImageUploadHelper.uploadCloudinary(multipartFile, user);
        } catch (MaxUploadSizeExceededException e) {
            throw new ResponseStatusException(HttpStatus.INSUFFICIENT_STORAGE, "Please upload an image which is under 1048576 bytes.");
        }
        ContestDTO contestDTO = new ContestDTO();
        contestDTO.setTitle(contest_title);
        contestDTO.setCategoryId(contest_category);
        Contest contest = contestMapper.dtoToObject(contestDTO, user);
        contest.setPhoto(image);
        try {
            contestService.create(contest);
        } catch(DuplicateEntityException e){
            model.addAttribute("message","The title of the contest is duplicated");
            return "contest-new";
        }

        //Create phase 1
        ContestPhase contestPhase1 = new ContestPhase();
        contestPhase1.setContest(contest);
        //SimpleDateFormat formatter6=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss a", Locale.US);
        contestPhase1.setPhaseType(phaseTypeService.getPhaseTypeByID(1));
        ZoneId zId=ZoneId.of("Europe/Sofia");
        LocalDateTime phase1_start_local = LocalDateTime.parse(phase1StartDate,
                DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
        contestPhase1.setStart_time(phase1_start_local);
        LocalDateTime phase1_end_local = LocalDateTime.parse(phase1EndDate,
                DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
        contestPhase1.setEnd_time(phase1_end_local);
        contestPhaseService.create(contestPhase1);

        //Create phase 2
        ContestPhase contestPhase2 = new ContestPhase();
        contestPhase2.setContest(contest);
        contestPhase2.setPhaseType(phaseTypeService.getPhaseTypeByID(2));
        LocalDateTime phase2_start_local = phase1_end_local;
        contestPhase2.setStart_time(phase2_start_local);
        LocalDateTime phase2_end_local = LocalDateTime.parse(phase2EndDate,
                DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
        contestPhase2.setEnd_time(phase2_end_local);
        contestPhaseService.create(contestPhase2);

        //Create phase 3
        ContestPhase contestPhase3 = new ContestPhase();
        contestPhase3.setContest(contest);
        contestPhase3.setPhaseType(phaseTypeService.getPhaseTypeByID(3));
        LocalDateTime phase3_start_local = LocalDateTime.parse(phase2EndDate,
                DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
        contestPhase3.setStart_time(phase3_start_local);
        LocalDateTime phase3_end_local = LocalDateTime.parse(phase2EndDate,
                DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
        contestPhase3.setEnd_time(phase3_end_local);
        contestPhaseService.create(contestPhase3);

        return "index";
    }

//    TODO delete? AV ok
  /*  @GetMapping("/{id}/update")
    public String showEditContestPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Contest contest = contestService.getById(id);
            ContestDTO contestDTO = contestMapper.toDto(contest);
            model.addAttribute("contestId", id);
            model.addAttribute("contest", contestDTO);
            return "contest-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }*/

//    TODO delete commented? AV ok
  /*  @PostMapping("/{id}/update")
    public String updatePost(@PathVariable int id,
                             @Valid @ModelAttribute("contest") ContestDTO dto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "contest-update";
        }
        try {
            Contest contest = contestMapper.fromDto(dto, id);
            contestService.update(contest, user);

            return "redirect:/my-contests-organizer";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "duplicate_contest", e.getMessage());
            return "contest-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
*/

    @GetMapping("/myorganizedcontests")
    public String showContestsIOrganized(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        int userID = user.getUser_id();
        List<Contest> result = contestService.getByOrganizer(user);
        model.addAttribute("myContestsOrganizer", result);
        model.addAttribute("hide_filter", 0);
        return "my-contests-organizer";
    }

//    TODO delete commented? AV ok
//    @GetMapping("/allcontests")
//    public String showAllContests(Model model, HttpSession session, Optional<String> title,
//                                  Optional<String> categoryName,
//                                  Optional<Integer> phaseId,
//                                  Optional<ContestSortOptions> sort) {
//        User user;
//        try {
//            user = authenticationHelper.tryGetUser(session);
//        } catch (AuthenticationFailureException e) {
//            model.addAttribute("auth_error",e.getMessage());
//            return "redirect:/login";
//        }
//        List<ContestWithPhase> result = contestService.getAll( title,
//                categoryName, phaseId,
//                sort);
//        model.addAttribute("contests", result);
//        model.addAttribute("hide_filter",0);
//        return "all-contests";
//    }

    @GetMapping("/allcontests")
    public String showAllContests(Model model, HttpSession session, Optional<String> title,
                                  Optional<String> categoryName,
                                  Optional<Integer> phaseId,
                                  Optional<ContestSortOptions> sort) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        List result = contestService.getAllContestsDetailed(user);
        model.addAttribute("contests", result);
        model.addAttribute("all_authors", contestService.getContestOrganizers());
        model.addAttribute("all_phases", contestPhaseService.getPhaseTypes());
        model.addAttribute("hide_filter", 0);
        return "all-contests";
    }

    @GetMapping("/contestsToJudge")
    public String showAllContestsToJudge(Model model, HttpSession session, Optional<String> title,
                                  Optional<String> categoryName,
                                  Optional<Integer> phaseId,
                                  Optional<ContestSortOptions> sort) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
//        List result = contestService.getAllContestsDetailed(user);
        model.addAttribute("contestsToJudge", contestService.getContestToJudge(user));
//        model.addAttribute("all_authors", contestService.getContestOrganizers());
//        model.addAttribute("all_phases", contestPhaseService.getPhaseTypes());
        model.addAttribute("hide_filter", 0);
        return "all-contests-to-judge";
    }

    @PostMapping("/filter_all_contests")
    public String filterPhotoContests(Model model, HttpSession session,
                                      @RequestParam(value = "filter_title", required = false)
                                              String contest_title,
                                      @RequestParam(value = "filter_user", required = false) Integer user_id,
                                      @RequestParam(value = "filter_phases", required = false) Integer phase_id,
                                      @RequestParam(value = "filter_sort", required = false) String sort,
                                      @RequestParam(value = "filter_contests_judge", required = false) String contests_judge) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        int hide_filter = 0;

        if (!contest_title.isEmpty()) {
            hide_filter = 1;
            model.addAttribute("contest_title", contest_title);
        }

        if (!sort.isEmpty()) {
            hide_filter = 1;
            String sort_text = "";
            switch (sort) {
                case " ORDER BY c.creation_time DESC":
                    sort_text = "Newest to Oldest";
                case " ORDER BY c.creation_time ASC":
                    sort_text = "Oldest to Newest";
            }
            model.addAttribute("sort_text", sort_text);
        }

        if (user_id != null) {
            hide_filter = 1;
            model.addAttribute("user", userService.getById(user_id, user).getUsername());
        }

        if (phase_id != null) {
            hide_filter = 1;
            model.addAttribute("phase", phaseTypeService.getPhaseTypeByID(phase_id).getPhase_type());
        }

        if (contests_judge.equals("Jury")) {
            hide_filter = 1;
            model.addAttribute("contests_judge", "Contests I need to judge.");
        }

//        model.addAttribute("all_authors",postService.getPostAuthors());
        model.addAttribute("contests", contestService.filterAllPostsCustom(Optional.ofNullable(contest_title), Optional.ofNullable(user_id), Optional.ofNullable(phase_id), Optional.ofNullable(sort), Optional.ofNullable(contests_judge), session));
        model.addAttribute("hide_filter", hide_filter);
        return "all-contests";
    }

    @GetMapping("/new_submission")
    public String showNewSubmissionForm(Model model, HttpSession session, @RequestParam int id) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        Contest contest = contestService.getById(id);
        try {
            photoSubmissionService.getByContestId(user.getUser_id(), contest.getContest_id());
        } catch (DuplicateEntityException e) {
            model.addAttribute("contests", contestService.getAllContestsDetailed(user));
            model.addAttribute("message", "You have already made a submission");
            return "all-contests";
        }
        if (contest.getOrganizer_id().getUsername().equals(user.getUsername())) {
            model.addAttribute("message", "As an organizer, you cannot participate");
        }
        if (contestService.isJury(contest.getContest_id(), user) == true) {
            model.addAttribute("message", "As a juror, you cannot participate");
        }
        session.setAttribute("contestID", contest.getContest_id());
        model.addAttribute("contest", contest);
        return "new-submission";
    }

    @PostMapping("/new_submission")
    public String create(Model model, HttpSession session, @RequestHeader HttpHeaders headers, @RequestParam("image") MultipartFile multipartFile,
                         @RequestParam("title") String title, @RequestParam("story") String story) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(session);
            Image image2;
            try {
                image2 = ImageUploadHelper.uploadCloudinary(multipartFile, loggedUser);
            } catch (MaxUploadSizeExceededException e) {
                throw new ResponseStatusException(HttpStatus.INSUFFICIENT_STORAGE,
                        "Please upload an image which is under 1048576 bytes.");
            }
            PhotoSubmissionDTO photoSubmissionDTO = new PhotoSubmissionDTO();
            photoSubmissionDTO.setTitle(title);
            photoSubmissionDTO.setStory(story);
            photoSubmissionDTO.setContest_id((Integer) session.getAttribute("contestID"));
            PhotoSubmission photoSubmission = photoSubmissionMapper.dtoToObject(photoSubmissionDTO, loggedUser, image2);
            photoSubmissionService.create(photoSubmission);
            contestService.addParticipants((Integer) session.getAttribute("contestID"), loggedUser);
        } catch (DuplicateEntityException e) {
            model.addAttribute("message", e.getMessage());
            model.addAttribute("contest", contestService.getById((Integer) session.getAttribute("contestID")));
            return "new-submission";
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } finally {
            model.addAttribute("contests", contestService.getAllContestsDetailed(authenticationHelper.tryGetUser(session)));
            return "all-contests";
        }
    }

    @GetMapping("/delete_contest")
    public String deleteContest(HttpSession session, Model model, @RequestParam int id,
                                Optional<String> title,
                                Optional<String> categoryName,
                                Optional<Integer> phaseId,
                                Optional<ContestSortOptions> sort) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }

        scoreService.DeleteAllByContestID(id);
        List<PhotoSubmission> photoSubmissions = photoSubmissionService.getPhotoSubmissionsByContestId(id);
        for (PhotoSubmission photoSubmission : photoSubmissions) {
            Set<Comment> comments = photoSubmission.getComments();
            for (Comment comment : comments) {
                commentService.deleteByID(comment.getComment_id());
            }
        }
        photoSubmissionService.deleteByContestID(id);
        for (PhotoSubmission photoSubmission : photoSubmissions) {
            imageService.deleteImage(photoSubmission.getPhoto().getImage_id());
        }


        contestPhaseService.deleteByContestID(id, user);
        contestService.delete(id, user);
        model.addAttribute("message", "Contest was successfully deleted");
        model.addAttribute("contests", contestService.getAllContestsDetailed(authenticationHelper.tryGetUser(session)));
        return "all-contests";
    }

    @GetMapping("/update")
    public String showEditContestPage(@RequestParam int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Contest contest = contestService.getById(id);
            ContestDTO contestDTO = contestMapper.toDto(contest);
            LocalDateTime ph1_start = contestPhaseService.getPhaseOneByContestID(id).getStart_time();
            LocalDateTime ph1_end = contestPhaseService.getPhaseOneByContestID(id).getEnd_time();
            LocalDateTime ph2_end = contestPhaseService.getPhaseTwoByContestID(id).getEnd_time();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH);
            String formattedStringPhase1 = ph1_start.format(formatter);
            String formattedStringPhase1End = ph1_end.format(formatter);
            String formattedStringPhase2 = ph2_end.format(formatter);


            model.addAttribute("contestId", valueOf(id));
            model.addAttribute("contest", contestDTO);
            model.addAttribute("localDateTime", LocalDateTime.now());
            model.addAttribute("phase1starttime", formattedStringPhase1);
            model.addAttribute("phase1endtime", formattedStringPhase1End);
            model.addAttribute("phase2", formattedStringPhase2);
            model.addAttribute("image", contest.getPhoto().getImage_link());
            session.setAttribute("previousImageId", contest.getPhoto().getImage_id());
            return "update-contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/update")
    public String updatePost(@Valid @ModelAttribute("contest") ContestDTO dto,
                             @RequestParam int id,
                             @RequestParam(value = "phase1_start_time") String phase1StartDate,
                             @RequestParam(value = "phase1_end_time") String phase1EndDate,
                             @RequestParam(value = "phase2_end_time") String phase2EndDate,
                             BindingResult errors,
                             Model model,
                             HttpSession session,
                             @RequestParam("image") MultipartFile multipartFile) throws IOException, ParseException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "update-contest";
        }
        try {
            Image image;
            if (!multipartFile.isEmpty()) {
                image = ImageUploadHelper.uploadCloudinary(multipartFile, user);
            } else {
                image = imageService.getImageById((Integer) session.getAttribute("previousImageId"));
            }
            Contest contest = contestMapper.fromDto(dto, id);
            contest.setPhoto(image);
            contestService.update(contest, user);

            //Create phase 1
            ContestPhase contestPhase1 = contestPhaseService.getPhaseOneByContestID(id);
            //SimpleDateFormat formatter6=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss a", Locale.US);
            contestPhase1.setPhaseType(phaseTypeService.getPhaseTypeByID(1));
            LocalDateTime phase1_start_local = LocalDateTime.parse(phase1StartDate,
                    DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
            contestPhase1.setStart_time(phase1_start_local);
            LocalDateTime phase1_end_local = LocalDateTime.parse(phase1EndDate,
                    DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
            contestPhase1.setEnd_time(phase1_end_local);
            contestPhaseService.update(contestPhase1, contest, user);

            //Create phase 2
            ContestPhase contestPhase2 = contestPhaseService.getPhaseTwoByContestID(id);
            contestPhase2.setPhaseType(phaseTypeService.getPhaseTypeByID(2));
            LocalDateTime phase2_start_local = phase1_end_local;
            contestPhase2.setStart_time(phase2_start_local);
            LocalDateTime phase2_end_local = LocalDateTime.parse(phase2EndDate,
                    DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
            contestPhase2.setEnd_time(phase2_end_local);
            contestPhaseService.update(contestPhase2, contest, user);

            //Create phase 3
            ContestPhase contestPhase3 = contestPhaseService.getPhaseThreeByContestID(id);
            contestPhase3.setPhaseType(phaseTypeService.getPhaseTypeByID(3));
            LocalDateTime phase3_start_local = LocalDateTime.parse(phase2EndDate,
                    DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
            contestPhase3.setStart_time(phase3_start_local);
            LocalDateTime phase3_end_local = LocalDateTime.parse(phase2EndDate,
                    DateTimeFormatter.ofPattern("MM/dd/yyyy h:mm a", Locale.ENGLISH));
            contestPhase3.setEnd_time(phase3_end_local.plusYears(1));
            contestPhaseService.update(contestPhase3, contest, user);
            return "redirect:/";
        } /*catch (DuplicateEntityException e) {
            errors.rejectValue("title", "duplicate_contest", e.getMessage());
            return "update-contest";
        }*/
//        } catch (DuplicateEntityException e) {
//            errors.rejectValue("title", "duplicate_contest", e.getMessage());
//            return "update-contest";
        catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/contest")
    public String showSingleContest(@RequestParam int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Contest contest = contestService.getById(id);
            LocalDateTime ph1_start = contestPhaseService.getPhaseOneByContestID(id).getStart_time();
            LocalDateTime ph1_end = contestPhaseService.getPhaseOneByContestID(id).getEnd_time();
            LocalDateTime ph2_end = contestPhaseService.getPhaseTwoByContestID(id).getEnd_time();
            PhotoSubmission photoSubmission = photoSubmissionService.getByContestId(user.getUser_id(), id);
            List<PhotoSubmission> photoSubmissionsWithoutUsers = photoSubmissionService.getPhotoSubmissionsByContestId(id);
            photoSubmissionsWithoutUsers.remove(photoSubmission);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy HH:MM:SS", Locale.ENGLISH);

            String formattedStringPhase2 = ph2_end.format(formatter);
            String formattedStringPhase1 = ph1_end.format(formatter);

            LocalDateTime localDateTime = LocalDateTime.now();
            model.addAttribute("contest", contest);
            model.addAttribute("image", contest.getPhoto().getImage_link());
            model.addAttribute("photoSubmissions", photoSubmissionService.getPhotoSubmissionsByContestId(id));
            model.addAttribute("photoSubmissionsWithoutUser", photoSubmissionsWithoutUsers);

            model.addAttribute("phase2",ph2_end);
            model.addAttribute("myPhoto", photoSubmission);
            model.addAttribute("isRunning", !contest.getIs_finished());
            model.addAttribute("isFinished", contest.getIs_finished());
            model.addAttribute("isFinishedJS", localDateTime.compareTo(ph2_end) > 0);

            boolean isPhase1 = localDateTime.compareTo(ph1_start) >= 0 && localDateTime.compareTo(ph1_end) <= 0;
            model.addAttribute("isPhase1",
                    isPhase1);
            model.addAttribute("isPhase2", localDateTime.compareTo(ph1_end) >= 0);
            model.addAttribute("phase2",isPhase1? formattedStringPhase1:formattedStringPhase2);

            boolean flag = (contestService.isParticipant(id, user)
                    || contestService.isJury(id, user))
                    || (contest.getOrganizer_id().getUser_id() == user.getUser_id());
            model.addAttribute("isParticipant", flag);
            model.addAttribute("isParticipantUser", contestService.isParticipant(id, user));


            return "contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/winners")
    public String showWinners(@RequestParam int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Contest contest = contestService.getById(id);
            LocalDateTime localDateTime = LocalDateTime.now();
            model.addAttribute("contest", contest);
            model.addAttribute("photoSubmissions1", contest.getFirstPlaceWinners());
            model.addAttribute("photoSubmissions2", contest.getSecondPlaceWinners());
            model.addAttribute("photoSubmissions3", contest.getThirdPlaceWinners());

            model.addAttribute("isRunning", !contest.getIs_finished());

            boolean flag = (contestService.isParticipant(id, user)
                    || contestService.isJury(id, user))
                    || (contest.getOrganizer_id().getUser_id() == user.getUser_id());
            model.addAttribute("isParticipant", flag);
            model.addAttribute("isParticipantUser", contestService.isParticipant(id, user));


            return "winners";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/view_submission")
    public String viewSubmission(Model model, HttpSession session, @RequestParam int id) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        PhotoSubmission photoSubmission = photoSubmissionService.getByID(id);
        Boolean isJury = contestService.isJury(photoSubmission.getContest().getContest_id(), user);
        model.addAttribute("comments", commentService.getBySubmissionID(id));
        model.addAttribute("photosubmission", photoSubmission);
        model.addAttribute("isJury", isJury);
        model.addAttribute("phaseID", contestPhaseService.getContestPhase(photoSubmission.getContest().getContest_id()));
        model.addAttribute("ratedByActiveUser", scoreService.getScoreByUserAndSubmission(user.getUser_id(), id));
        return "view-submission";
    }


    @GetMapping("/submit_score")
    public String submit(HttpSession session, Model model, @RequestParam int id) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        session.setAttribute("photosubmission", id);
        model.addAttribute("photosubmission", photoSubmissionService.getByID(id));
        return "submit_score";
    }

    @PostMapping("/submit_score")
    public String submit(Model model,
                         @RequestParam(value = "fits_category",required = false) String fits_category,
                         @RequestParam(value = "photo-rating") Integer photo_rating,
                         @RequestParam(value = "comment") String comment,
                         HttpSession session
    ) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        int photosubmission_id = (int) session.getAttribute("photosubmission");
        CommentDTO commentDTO = new CommentDTO();
        String moderated_comment= moderationHelperComments.moderateText(comment);
        commentDTO.setContent(moderated_comment);
        commentDTO.setPhotoSubmission_id(photosubmission_id);
        Comment comment1 = commentMapper.dtoToObject(commentDTO, user);
        commentService.create(comment1);
        ScoreDTO scoreDTO = new ScoreDTO();
        scoreDTO.setScore(photo_rating);
        if (fits_category == "No") {
            scoreDTO.setScore(0);
        }
        Score score = scoreMapper.dtoToObject(scoreDTO, user, photoSubmissionService.getByID(photosubmission_id));
        scoreService.create(score);
        int Contest_id = photoSubmissionService.getByID(photosubmission_id).getContest().getContest_id();
        String redirect_link = String.format("redirect:/contest?id=%s", String.valueOf(Contest_id));
        return redirect_link;
    }


    @GetMapping("/list_potential_jurors")
    public String showPotentialJurorsList(@RequestParam(value = "contest_id") int contest_id,
                                          HttpSession session,
                                          Model model) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("contest_id", contest_id);
        try {
            model.addAttribute("users",
//                    We can play with roleId and minValue before we have real users who are eligible
//                    TODO replace minIdValue with 3 when we have eligible users (as per requirement)
                    userService.getActiveWithRoleIdAndBadgeAtLeast(1, 2, initiator));
            Contest contest = contestService.getById(contest_id);
            model.addAttribute("contest", contest);
            return "potential-jury-list";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error_access_denied", e.getMessage());
            return "access-denied";
        }catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/addjuror")
    public String addJurors(HttpSession session,
                            @RequestParam(value = "contest_id") int contest_id,
                            @RequestParam(value = "user_id") int user_id,
                            Model model) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        model.addAttribute("contest_id", contest_id);
        try {
            User userToAdd = userService.getById(user_id, initiator);
            contestService.addJurors(contest_id, userToAdd, initiator);
        } catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        String redirectString = "redirect:/list_potential_jurors?contest_id="+contest_id;
        return redirectString;
    }

    @GetMapping("/removejuror")
    public String removeJurors(HttpSession session,
                               @RequestParam(value = "contest_id") int contest_id,
                               @RequestParam(value = "user_id") int user_id,
                               Model model) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        model.addAttribute("contest_id", contest_id);
        try {
            User userToAdd = userService.getById(user_id, initiator);
            contestService.removeJurors(contest_id, userToAdd, initiator);
            model.addAttribute("user", userToAdd);
        } catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        String redirectString = "redirect:/list_potential_jurors?contest_id="+contest_id;
        return redirectString;
    }

    @GetMapping("/list_jurors")
    public String showJurorsList(@RequestParam(value = "contest_id") int contest_id,
                                 HttpSession session,
                                 Model model) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("contest_id", contest_id);
        try {
            Contest contest = contestService.getById(contest_id);
            model.addAttribute("users",
                    contest.getJurors());
            return "jury-list";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error_access_denied", e.getMessage());
            return "access-denied";
        } catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/moderate_photos")
    public String showadultratedphotos(HttpSession session,Model model){
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("photos",imageService.getAdultImages());
        return "moderate-photos";
    }

    @GetMapping("/replace_inappropriate_image")
    public String replaceInapprorpiateImage(HttpSession session,Model model,@RequestParam(value = "id") int id){
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        Image censored_image = imageService.getImageById(id);
        censored_image.setImage_rating(1);
        censored_image.setImage_link("https://res.cloudinary.com/dsrlmhn6d/image/upload/v1650716453/censored-gdfbdcd1ef_1920_yrebog.jpg");
        imageService.censorImage(censored_image);

        Image profilePictureSessionUpdate = imageService.getImageById(initiator.getPhoto().getImage_id());
        session.setAttribute("pictureLink", profilePictureSessionUpdate.getImage_link());

        model.addAttribute("photos",imageService.getAdultImages());
        return "moderate-photos";
    }
}
