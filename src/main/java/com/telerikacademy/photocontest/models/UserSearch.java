package com.telerikacademy.photocontest.models;

public class UserSearch {
    private String search;

    public UserSearch() {
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
