package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name="contest_phases")

public class ContestPhase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_phase_id")
    private int contest_phase_id;

    @ManyToOne
    @JoinColumn(name = "phase_id")
    private PhaseType phaseType;

    @Column(name = "start_time")
    @NotNull
    private LocalDateTime start_time;

    @Column(name = "end_time")
    @NotNull
    private LocalDateTime end_time;

    @OneToOne
    @JoinColumn(name = "cont_id")
    @NotNull
    private Contest contest;

    public ContestPhase(int contest_phase_id, PhaseType phaseType, Contest contest,
                        LocalDateTime start_time, LocalDateTime end_time) {
        this.contest_phase_id = contest_phase_id;
        this.phaseType = phaseType;
        this.contest = contest;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public ContestPhase() {

    }

    public int getContest_phase_id() {
        return contest_phase_id;
    }

    public void setContest_phase_id(int contest_phase_id) {
        this.contest_phase_id = contest_phase_id;
    }

    public PhaseType getPhaseType() {
        return phaseType;
    }

    public void setPhaseType(PhaseType phaseType) {
        this.phaseType = phaseType;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public LocalDateTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDateTime start_time) {
        this.start_time = start_time;
    }

    public LocalDateTime getEnd_time() {
        return end_time;
    }

    public void setEnd_time(LocalDateTime end_time) {
        this.end_time = end_time;
    }

    @Override
    public String toString() {
        return "ContestPhase{" +
                "contest_phase_id=" + contest_phase_id +
                ", phaseType=" + phaseType +
                ", start_time=" + start_time +
                ", end_time=" + end_time +
                ", contest=" + contest +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestPhase that = (ContestPhase) o;
        return contest_phase_id == that.contest_phase_id && phaseType.equals(that.phaseType) && start_time.equals(that.start_time) && end_time.equals(that.end_time) && contest.equals(that.contest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contest_phase_id, phaseType, start_time, end_time, contest);
    }
}
