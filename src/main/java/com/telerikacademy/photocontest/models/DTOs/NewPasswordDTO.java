package com.telerikacademy.photocontest.models.DTOs;

import org.springframework.stereotype.Component;

import javax.validation.constraints.Size;

public class NewPasswordDTO {
    @Size(min = 4, message = "The password should contain minimum 4 symbols.")
    private String newPassword;

    @Size(min = 4, message = "The password should contain minimum 4 symbols.")
    private String newPasswordConfirmation;

    public NewPasswordDTO(String newPassword, String newPasswordConfirmation) {
        this.newPassword = newPassword;
        this.newPasswordConfirmation = newPasswordConfirmation;
    }

    public NewPasswordDTO() {
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirmation() {
        return newPasswordConfirmation;
    }

    public void setNewPasswordConfirmation(String newPasswordConfirmation) {
        this.newPasswordConfirmation = newPasswordConfirmation;
    }
}
