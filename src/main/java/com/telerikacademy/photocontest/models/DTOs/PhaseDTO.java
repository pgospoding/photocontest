package com.telerikacademy.photocontest.models.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class PhaseDTO {
    @JsonProperty(value = "phaseType")
    private int phaseType;

    private LocalDateTime start_time;

    private LocalDateTime end_time;

    public PhaseDTO() {
    }

    public PhaseDTO(int phaseType, LocalDateTime start_time, LocalDateTime end_time) {
        this.phaseType = phaseType;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public int getPhaseType() {
        return phaseType;
    }

    public void setPhaseType(int phaseType) {
        this.phaseType = phaseType;
    }

    public LocalDateTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDateTime start_time) {
        this.start_time = start_time;
    }

    public LocalDateTime getEnd_time() {
        return end_time;
    }

    public void setEnd_time(LocalDateTime end_time) {
        this.end_time = end_time;
    }
}
