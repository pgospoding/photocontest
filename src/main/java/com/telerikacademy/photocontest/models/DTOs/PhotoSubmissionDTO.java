package com.telerikacademy.photocontest.models.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PhotoSubmissionDTO {

    @NotNull(message = "Title cannot be null.")
    @Size(max = 64, message = "Title shouldn't be longer than 64 characters.")
    private String title;

    @NotNull(message = "Story cannot be null.")
    private String story;

    @NotNull(message = "Contest id cannot be null.")
    private Integer contest_id;

    public PhotoSubmissionDTO() {
    }

    public PhotoSubmissionDTO(String title, String story, int contest_id) {
        this.title = title;
        this.story = story;
        this.contest_id = contest_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Integer getContest_id() {
        return contest_id;
    }

    public void setContest_id(Integer contest_id) {
        this.contest_id = contest_id;
    }
}
