package com.telerikacademy.photocontest.models.DTOs;


import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SelfUpdDTO {

    @Size(min = 4, max = 32, message = "Minimum first name length is 4 symbols, maximum first name length is 32 symbols")
    private String first_name;

    @Size(min = 4, max = 32, message = "Minimum last name length is 4 symbols, maximum last name length is 32 symbols")
    private String last_name;

    @Pattern(regexp = "^(.+)@(.+)$", message = "Email should contain @")
    private String email;

//the user shouldn't be able to change username
//the user shouldn't update his own role or status

    //constructor with everything
    private SelfUpdDTO(String first_name,
                       String last_name,
                       String email) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;

    }


    public SelfUpdDTO() {
    }


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
