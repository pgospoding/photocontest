package com.telerikacademy.photocontest.models.DTOs;

import javax.validation.constraints.Size;

public class ByIdNewPasswordDTO {
    @Size(min = 2, message = "The username must be 2 or more symbols long.")
    private String username;

    @Size(min = 4, message = "The password should contain minimum 4 symbols.")
    private String newPassword;

    @Size(min = 4, message = "The password should contain minimum 4 symbols.")
    private String newPasswordConfirmation;

    public ByIdNewPasswordDTO(String username, String newPassword, String newPasswordConfirmation) {
        this.username = username;
        this.newPassword = newPassword;
        this.newPasswordConfirmation = newPasswordConfirmation;
    }

    public ByIdNewPasswordDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirmation() {
        return newPasswordConfirmation;
    }

    public void setNewPasswordConfirmation(String newPasswordConfirmation) {
        this.newPasswordConfirmation = newPasswordConfirmation;
    }
}
