package com.telerikacademy.photocontest.models.DTOs;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Component
public class ScoreDTO {

    @NotNull(message = "Score cannot be null.")
    @Size(min = 0,max = 10, message = "Score should be between 0 and 10")
    private int score;

    public ScoreDTO(int score) {
        this.score = score;
    }

    public ScoreDTO() {
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
