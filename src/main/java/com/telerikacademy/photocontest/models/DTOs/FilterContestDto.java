package com.telerikacademy.photocontest.models.DTOs;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;

public class FilterContestDto {
    private Integer phaseId;
    private String title;
    private Integer categoryId;
    private String contestSortOption;

    public FilterContestDto() {
    }

    public FilterContestDto(Integer phaseId,
                            String title,
                            Integer categoryId,
                            String contestSortOption) {
        this.phaseId = phaseId;
        this.title = title;
        this.categoryId = categoryId;
        this.contestSortOption = contestSortOption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(Integer phaseId) {
        this.phaseId = phaseId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getContestSortOption() {
        return contestSortOption;
    }

    public void setContestSortOption(String contestSortOption) {
        this.contestSortOption = contestSortOption;
    }
}