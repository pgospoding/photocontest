package com.telerikacademy.photocontest.models.DTOs;

import com.telerikacademy.photocontest.models.RoleType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserCreateDTO {

    @NotNull
    @Size(min = 4, max = 32, message = "Minimum first name length is 4 symbols, maximum first name length is 32 symbols")
    private String first_name;

    @NotNull
    @Size(min = 4, max = 32, message = "Minimum last name length is 4 symbols, maximum first name length is 32 symbols")
    private String last_name;

    @NotNull
    @Pattern(regexp = "^(.+)@(.+)$", message = "Email should contain @")
    private String email;

    @NotNull
    @Size(min = 2, message = "The username must be 2 or more symbols long.")
    private String username;

    @NotNull
    @Size(min = 4, message = "The password should contain minimum 4 symbols.")
    private String password;

    @NotNull
    @Size(min = 4, message = "The password should contain minimum 4 symbols.")
    private String passwordConfirmation;


    public UserCreateDTO(String first_name,
                         String last_name,
                         String email,
                         String username,
                         String password,
                         String passwordConfirmation) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public UserCreateDTO() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
