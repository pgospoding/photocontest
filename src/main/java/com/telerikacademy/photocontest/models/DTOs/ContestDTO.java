package com.telerikacademy.photocontest.models.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.telerikacademy.photocontest.models.CategoryType;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ContestDTO {
    @JsonProperty(value = "title")
    @NotNull(message = "Title cannot be null.")
    @Size(max = 64, message = "Title shouldn't be longer than 64 characters.")
    private String title;

/*
    @Positive(message = "Category ID should be positive")
*/
@JsonProperty(value = "category")
private int categoryType;

/*
    @Positive(message = "Image ID should be positive")
*/

    public ContestDTO() {
    }

    public ContestDTO(String title, int categoryId) {
        this.title = title;
        this.categoryType = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryId() {
        return categoryType;
    }

    public void setCategoryId(int categoryId) {
        this.categoryType = categoryId;
    }

}
