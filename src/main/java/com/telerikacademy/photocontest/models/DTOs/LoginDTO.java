package com.telerikacademy.photocontest.models.DTOs;

import javax.validation.constraints.Size;

public class LoginDTO {
    @Size(min = 2, message = "The username must be 2 or more symbols long.")
    private String username;

    @Size(min = 4, message = "The password should contain minimum 4 symbols.")
    private String password;

    public LoginDTO() {
    }

    public LoginDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

