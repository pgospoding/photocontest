package com.telerikacademy.photocontest.models.DTOs;

import javax.validation.constraints.NotNull;

public class CommentUpdateDTO {

    @NotNull(message = "Comment cannot be null.")
    private String content;


    public CommentUpdateDTO() {
    }

    public CommentUpdateDTO(String content, Integer photoSubmission_id) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
}
