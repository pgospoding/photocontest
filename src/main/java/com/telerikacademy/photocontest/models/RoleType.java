package com.telerikacademy.photocontest.models;

import javax.management.relation.Role;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "role_type")
public class RoleType {

    public static final Map<String, RoleType> userRoles = new HashMap<>();
    public static final RoleType USER = new RoleType(1, "User");
    public static final RoleType ORGANIZER = new RoleType(2, "Organizer");
    public static final RoleType ADMIN = new RoleType(3, "Admin");
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_type_id")
    private int role_id;

    @Column(name = "role")
    @NotNull
    @Size(max = 132, message = "Maximum is 132 symbols")
    private String role;

    public RoleType(int role_id, String role) {
        this.role_id = role_id;
        this.role = role;
        userRoles.put(role, this);
    }

    public RoleType() {

    }
    public static RoleType valueOf(String roleName) {
        return userRoles.get(roleName);
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    @Override
    public String toString() {
        return "RoleType{" +
                "role_id=" + role_id +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleType roleType = (RoleType) o;
        return role_id == roleType.role_id && role.equals(roleType.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(role_id);
    }
}
