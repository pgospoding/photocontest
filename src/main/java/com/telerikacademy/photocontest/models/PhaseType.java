package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name="phase_type")

public class PhaseType {
    public static final Map<String, PhaseType> phasesNames = new HashMap<>();
    public static final Map<Integer, PhaseType> phasesIds = new HashMap<>();
    public static final PhaseType NOT_STARTED = new PhaseType(0, "Not Started");
    public static final PhaseType PHASE1 = new PhaseType(1, "Phase1");
    public static final PhaseType PHASE2 = new PhaseType(2, "Phase2");
    public static final PhaseType FINISHED = new PhaseType(3, "Finished");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phase_type_id")
    private int phase_type_id;

    @Column(name = "phase_type")
    @NotNull
    private String phase_type;

    public PhaseType(int phase_type_id, String phase_type) {
        this.phase_type_id = phase_type_id;
        this.phase_type = phase_type;
        phasesNames.put(phase_type,this);
        phasesIds.put(phase_type_id,this);
    }

    public PhaseType() {

    }

    public int getPhase_type_id() {
        return phase_type_id;
    }

    public void setPhase_type_id(int phase_type_id) {
        this.phase_type_id = phase_type_id;
    }

    public String getPhase_type() {
        return phase_type;
    }

    public void setPhase_type(String phase_type) {
        this.phase_type = phase_type;
    }

    public static PhaseType valueOf(String phaseTypeName){
        return phasesNames.get(phaseTypeName);
    }

    public static PhaseType valueOf(int phaseTypeId){
        return phasesNames.get(phaseTypeId);
    }

    @Override
    public String toString() {
        return "PhaseType{" +
                "phase_type_id=" + phase_type_id +
                ", phase_type='" + phase_type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhaseType phaseType = (PhaseType) o;
        return phase_type_id == phaseType.phase_type_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(phase_type_id);
    }
}
