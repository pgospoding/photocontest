package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name="comments")

public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int comment_id;

    @Column(name = "content")
    @NotNull
    private String content;

    @ManyToOne
    @JoinColumn(name = "photo_submission_id")
    private PhotoSubmission photoSubmission;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "comment_creation_datetime")
    @NotNull
    private LocalDateTime creation_time;

    public Comment(int comment_id, String content, PhotoSubmission photoSubmission, User user, LocalDateTime creation_time) {
        this.comment_id = comment_id;
        this.content = content;
        this.photoSubmission = photoSubmission;
        this.user = user;
        this.creation_time = creation_time;
    }

    public Comment() {

    }

    public LocalDateTime getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(LocalDateTime creation_time) {
        this.creation_time = creation_time;
    }

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public PhotoSubmission getPhotoSubmission() {
        return photoSubmission;
    }

    public void setPhotoSubmission(PhotoSubmission photoSubmission) {
        this.photoSubmission = photoSubmission;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
