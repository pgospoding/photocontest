package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Objects;

@Entity
@Table(name = "category_type")

public class CategoryType {

    public static final HashMap<String, CategoryType> categories = new HashMap<>();
    public static final HashMap<Integer, CategoryType> categoryIds = new HashMap<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_type_id")
    private int category_type_id;

    @Column(name = "category_type")
    @NotNull
    private String categoryType;

    public CategoryType(int category_type_id, String category_type) {
        this.category_type_id = category_type_id;
        this.categoryType = category_type;
        categories.put(category_type, this);
    }

    public CategoryType() {

    }

    public static CategoryType valueOf(String category_type) {
        return categories.get(category_type);
    }

    public static CategoryType valueOf(int categoryId) {
        return categoryIds.get(categoryId);
    }

    public int getCategory_type_id() {
        return category_type_id;
    }

    public void setCategory_type_id(int category_type_id) {
        this.category_type_id = category_type_id;
    }

    public String getCategory_type() {
        return categoryType;
    }

    public void setCategory_type(String category_type) {
        this.categoryType = category_type;
    }

    @Override
    public String toString() {
        return "CategoryType{" +
                "category_type_id=" + category_type_id +
                ", categoryType='" + categoryType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryType that = (CategoryType) o;
        return category_type_id == that.category_type_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(category_type_id);
    }
}
