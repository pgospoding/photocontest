package com.telerikacademy.photocontest.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum ContestSortOptions {
    TITLE_ASC("Title, ascending", " order by title "),
    TITLE_DESC("Title, descending", " order by title desc ");

    private final String preview;
    private final String query;

    private static final Map<String, ContestSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (ContestSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    ContestSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static ContestSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
