package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name="badge_type")

public class BadgeType {
    private static final Map<String, BadgeType> badges = new HashMap<>();
    private static final Map<Integer, BadgeType> badgeIds = new HashMap<>();
    public static final BadgeType JUNKIE = new BadgeType(1, "Junkie");
    public static final BadgeType ENTHUSIAST = new BadgeType(2,"Enthusiast");
    public static final BadgeType MASTER = new BadgeType(3, "Master");
    public static final BadgeType DICTATOR = new BadgeType(4, "Dictator");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "badge_type_id")
    private int badge_id;

    @Column(name = "badge_type")
    @NotNull
    @Size(max = 132, message = "Maximum is 132 symbols")
    private String badge_type;

    public BadgeType(int badge_id, String badge_type) {
        this.badge_id = badge_id;
        this.badge_type = badge_type;
        badges.put(badge_type,this);
        badgeIds.put(badge_id,this);
    }

    public BadgeType() {

    }

    public int getBadge_id() {
        return badge_id;
    }

    public void setBadge_id(int badge_id) {
        this.badge_id = badge_id;
    }

    public String getBadge_type() {
        return badge_type;
    }

    public void setBadge_type(String badge_type) {
        this.badge_type = badge_type;
    }

    public static BadgeType valueOf(String badge_type_name){
        return badges.get(badge_type_name);
    }

    public static BadgeType valueOf(Integer badge_id){
        return badgeIds.get(badge_id);
    }

    @Override
    public String toString() {
        return "BadgeType{" +
                "badge_id=" + badge_id +
                ", badge_type='" + badge_type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BadgeType badgeType = (BadgeType) o;
        return badge_id == badgeType.badge_id && badge_type.equals(badgeType.badge_type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(badge_id);
    }
}
