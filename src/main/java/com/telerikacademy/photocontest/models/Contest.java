package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Table(name="contests")
public class Contest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int contest_id;

    @Column(name = "title")
    @NotNull
    private String title;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryType categoryType;

    @Column(name = "creation_time")
    @NotNull
    private LocalDateTime creationTime;

    @Column(name = "is_finished")
    @NotNull
    private Boolean is_finished;

    @ManyToOne
    @JoinColumn(name = "organizer",nullable = false)
        private User organizerId;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "juries",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jurors;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_participants",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> participants;

    @JsonIgnore
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "first_place_winners",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "photo_submission_id")
    )
    private Set<PhotoSubmission> firstPlaceWinners;

    @JsonIgnore
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "second_place_winners",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "photo_submission_id")
    )
    private Set<PhotoSubmission> secondPlaceWinners;

    @JsonIgnore
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "third_place_winners",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "photo_submission_id")
    )
    private Set<PhotoSubmission> thirdPlaceWinners;

    @OneToOne
    @JoinColumn(name = "cover_photo")
    private Image photo;



    public Contest(int contest_id,
                   String title,
                   CategoryType categoryType,
                   LocalDateTime creationTime,
                   Boolean is_finished, User organizerId,
                   Set<User> jurors,
                   Set<User> participants,
                   Image photo) {
        this.contest_id = contest_id;
        this.title = title;
        this.categoryType = categoryType;
        this.creationTime = creationTime;
        this.is_finished = is_finished;
        this.organizerId = organizerId;
        this.jurors = jurors;
        this.participants = participants;
        this.photo = photo;
        this.firstPlaceWinners = new HashSet<>();
        this.secondPlaceWinners = new HashSet<>();
        this.thirdPlaceWinners = new HashSet<>();
    }

    public Contest() {

    }

    public int getContest_id() {
        return contest_id;
    }

    public void setContest_id(int contest_id) {
        this.contest_id = contest_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public LocalDateTime getCreation_time() {
        return creationTime;
    }

    public void setCreation_time(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public User getOrganizer_id() {
        return organizerId;
    }

    public void setOrganizer_id(User organizerId) {
        this.organizerId = organizerId;
    }

    public Set<User> getJurors() {
        return jurors;
    }

    public void setJurors(Set<User> jurors) {
        this.jurors = jurors;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public Boolean getIs_finished() {
        return is_finished;
    }

    public void setIs_finished(Boolean is_finished) {
        this.is_finished = is_finished;
    }

    public Set<PhotoSubmission> getFirstPlaceWinners() {
        return firstPlaceWinners;
    }

    public void setFirstPlaceWinners(Set<PhotoSubmission> firstPlaceWinners) {
        this.firstPlaceWinners = firstPlaceWinners;
    }

    public Set<PhotoSubmission> getSecondPlaceWinners() {
        return secondPlaceWinners;
    }

    public void setSecondPlaceWinners(Set<PhotoSubmission> secondPlaceWinners) {
        this.secondPlaceWinners = secondPlaceWinners;
    }

    public Set<PhotoSubmission> getThirdPlaceWinners() {
        return thirdPlaceWinners;
    }

    public void setThirdPlaceWinners(Set<PhotoSubmission> thirdPlaceWinners) {
        this.thirdPlaceWinners = thirdPlaceWinners;
    }

    @Override
    public String toString() {
        return "Contest{" +
                "contest_id=" + contest_id +
                ", title='" + title + '\'' +
                ", categoryType=" + categoryType +
                ", creationTime=" + creationTime +
                ", is_finished=" + is_finished +
                ", organizerId=" + organizerId +
                ", jurors=" + jurors +
                ", participants=" + participants +
                ", firstPlaceWinners=" + firstPlaceWinners +
                ", secondPlaceWinners=" + secondPlaceWinners +
                ", thirdPlaceWinners=" + thirdPlaceWinners +
                ", photo=" + photo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return contest_id == contest.contest_id && Objects.equals(title, contest.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contest_id, title);
    }
}
