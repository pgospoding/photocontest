package com.telerikacademy.photocontest.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User implements Comparable<User>{

    public static final int[] MAX_FOR_LEVELS = {-1, 50, 150, 1000};

    @Id
    //incrementation
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int user_id;

    @Column(name = "first_name")
    @NotNull
    @Size(min = 4, max = 32, message = "Minimum first name length is 4 symbols, maximum first name length is 32 symbols")
    private String first_name;

    @Column(name = "last_name")
    @NotNull
    @Size(min = 4, max = 32, message = "Minimum last name length is 4 symbols, maximum first name length is 32 symbols")
    private String last_name;

    @Column(name = "email")
    @NotNull
    @Pattern(regexp = "^(.+)@(.+)$", message = "Email should contain @")
    private String email;

    @Column(name = "username")
    @NotNull
    @Size(min = 2, message = "Minimum username length is 2 symbols")
    private String username;

    @Column(name = "password")
    @NotNull
    @Size(min = 4, message = "Minimum password length is 4 symbols")
    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private RoleType roleType;

    @Column(name = "total_points")
    private int totalPoints;

    @ManyToOne
    @JoinColumn(name = "badge_id")
    private BadgeType badgeType;


    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @OneToOne
    @JoinColumn(name = "image_id")
    private Image photo;

    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    //constructor with everything
    private User(int user_id,
                 String first_name,
                 String last_name,
                 String email,
                 String username,
                 String password,
                 RoleType roleType,
                 Status status,
                 Image photo) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.roleType = roleType;
        this.totalPoints = 0;
        this.badgeType = BadgeType.JUNKIE;
        this.status = status;
        this.photo = photo;
    }

    //constructor with null photo
    private User(int user_id,
                 String first_name,
                 String last_name,
                 String email,
                 String username,
                 String password,
                 RoleType roleType,
                 Status status) {
        this(user_id,
                first_name,
                last_name,
                email,
                username,
                password,
                roleType,
                status,
                null);
    }

    //constructor with all string values - and photo
    public User(int user_id,
                String first_name,
                String last_name,
                String email,
                String username,
                String password,
                String roleName,
                String statusName,
                Image photo) {
        this(user_id,
                first_name,
                last_name,
                email,
                username,
                password,
                RoleType.valueOf(roleName),
                Status.valueOf(statusName),
                photo);

    }

    //constructor with all String values and with null photo
    public User(int user_id,
                String first_name,
                String last_name,
                String email,
                String username,
                String password,
                String roleName,
                String statusName) {
        this(user_id,
                first_name,
                last_name,
                email,
                username,
                password,
                RoleType.valueOf(roleName),
                Status.valueOf(statusName),
                null);

    }


    public User() {
    }

    @JsonIgnore
    public boolean isActiveAdmin() {
        return roleType.getRole_id() == 3 && status.getStatus_id() == 1;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public void setRoleType(String roleTypeName) {
        setRoleType(RoleType.valueOf(roleTypeName));
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setStatus(String statusName) {
        setStatus(Status.valueOf(statusName));
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public BadgeType getBadgeType() {
        return badgeType;
    }

    public void setBadgeType(BadgeType badgeType) {
        this.badgeType = badgeType; 
    }

    public void setBadgeType() {
        for (int i = MAX_FOR_LEVELS.length - 1; i >= 0; i--) {
            if (totalPoints > MAX_FOR_LEVELS[i]) {
                setBadgeType(BadgeType.valueOf(i + 1));
                break;
            }
        }
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    @Override
    public String toString() {
        return "User{" +
                "user_id=" + user_id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roleType=" + roleType +
                ", badgeType=" + badgeType +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return user_id == user.user_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user_id);
    }

    @Override
    public int compareTo(User o) {
        if (this.user_id > o.user_id) {
            return 1;
        }
        else if (this.user_id < o.user_id) {
            return -1;
        }
        else {
            return 0;
        }
    }

}
