package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.telerikacademy.photocontest.repositories.contracts.ScoreRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PhotoSubmissionRepositoryImpl extends AbstractCRUDRepository<PhotoSubmission> implements PhotoSubmissionRepository {
    private final ScoreRepository scoreRepository;

    @Autowired
    public PhotoSubmissionRepositoryImpl(SessionFactory sessionFactory, ScoreRepository scoreRepository) {
        super(PhotoSubmission.class, sessionFactory);
        this.scoreRepository = scoreRepository;
    }

    @Override
    public List<PhotoSubmission> getAllByContestID(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery(" from PhotoSubmission where " +
                    "contest.contest_id = :contest_id ", PhotoSubmission.class);
            query.setParameter("contest_id", id);
            return query.list();
        }
    }

    @Override
    public List<PhotoSubmission> searchByTitle(Optional<String> filter) {

        if (filter.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery(
                    "from PhotoSubmission where title like :search ", PhotoSubmission.class
            );
            query.setParameter("search", "%" + filter.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<PhotoSubmission> getTop10PhotoSubmissionsByContestID(int contest_id) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery("from PhotoSubmission where contest.contest_id = :contest_id" +
                            " order by totalScore desc ", PhotoSubmission.class);
                    query.setParameter("contest_id", contest_id);
            return query.setMaxResults(10).list();
        }
    }


    @Override
    public void DeleteByContestID (int id){
        List<PhotoSubmission> result;
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery("from PhotoSubmission where contest.contest_id = :contest_id"
                    , PhotoSubmission.class);
            query.setParameter("contest_id", id);
            result = query.list();
        }
        for(PhotoSubmission photoSubmission : result){
            try(Session session3 = sessionFactory.openSession()){
                session3.beginTransaction();
                session3.delete(photoSubmission);
                session3.getTransaction().commit();
            }
        }
    }

    @Override
    public List<PhotoSubmission> getByUserID(int id){
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery(" from PhotoSubmission where " +
                    "user.user_id = :user_id ", PhotoSubmission.class);
            query.setParameter("user_id", id);
            return query.list();
        }
    }

    @Override
    public PhotoSubmission getByContestId(int id, int contestId){
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery(" from PhotoSubmission where " +
                    "user.user_id = :user_id and contest.contest_id =:contest_id", PhotoSubmission.class);
            query.setParameter("user_id", id);
            query.setParameter("contest_id", contestId);
            List<PhotoSubmission> result = query.list();
            if (result.size() != 0) {
                throw new DuplicateEntityException("", "", "");
            }
            return query.list().get(0);
        }
    }
    @Override
    public List<PhotoSubmission> getByContestIdandUser(int id, int contestId){
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery(" from PhotoSubmission where " +
                    "user.user_id = :user_id and contest.contest_id =:contest_id", PhotoSubmission.class);
            query.setParameter("user_id", id);
            query.setParameter("contest_id", contestId);
            List<PhotoSubmission> result = query.list();
            if (result.size() != 0) {
                throw new DuplicateEntityException("", "", "");
            }
            return query.list();
        }
    }
}
