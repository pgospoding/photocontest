package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepository<CategoryType> implements CategoryRepository {
    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(CategoryType.class, sessionFactory);
    }

}
