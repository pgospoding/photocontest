package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.Score;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.telerikacademy.photocontest.repositories.contracts.ScoreRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class ScoreRepositoryImpl extends AbstractCRUDRepository<Score> implements ScoreRepository {

    @Autowired
    public ScoreRepositoryImpl(SessionFactory sessionFactory) {
        super(Score.class, sessionFactory);
    }

    @Override
    public long getTotalByPhotoId(int photoId) {
        try(Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select sum(s.score) from Score as s where " +
                    " s.photoSubmission.photo_submission_id =: photo_id ");
            query.setParameter("photo_id", photoId);
            long totalScore = query.getSingleResult();
            return totalScore;
        }
    }

    @Override
    public void DeleteAllByContestID(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Score> query = session.createQuery("from Score where photoSubmission.contest.id = :contest_id"
                    , Score.class);
            query.setParameter("contest_id", id);
            List<Score> result = query.list();
            for(Score score : result){
                try(Session session2 = sessionFactory.openSession()){
                    session2.beginTransaction();
                    session2.delete(score);
                    session2.getTransaction().commit();
                }
            }
        }
    }

    @Override
    public Boolean getScoreByUserAndSubmission(int user_id, int submission_id){
        try (Session session = sessionFactory.openSession()) {
            Query<Score> query = session.createQuery("from Score where photoSubmission.id = :submission_id and user.id = :user_id"
                    , Score.class);
            query.setParameter("user_id", user_id);
            query.setParameter("submission_id", submission_id);
            List<Score> result = query.list();
            if(result.size()==0){
                return false;
            }
            return true;
        }
    }
}
