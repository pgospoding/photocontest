package com.telerikacademy.photocontest.repositories.contracts;

public interface BaseCreateDeleteRepository<T> extends BaseReadRepository<T> {
    T create(T entity);

    String delete(int id);

    String delete (T entity);

}
