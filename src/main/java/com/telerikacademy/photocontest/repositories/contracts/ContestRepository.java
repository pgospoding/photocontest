package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestWithPhase;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseCRUDRepository<Contest> {
    Contest getByTitle(String title);

    List<Contest> getByOrganizer(User user);

    List<Contest> filter(Optional<String> title,
                         Optional<Integer> categoryId,
                         Optional<Integer> phaseId,
                         Optional<ContestSortOptions> sort);

    List<ContestWithPhase> filterWithPhases(Optional<String> title,
                                            Optional<Integer> categoryId,
                                            Optional<Integer> contestPhaseId,
                                            Optional<ContestSortOptions> sort);

    List getFinishedContestsByParticipant(int participantId);

    List getRunningContestsByParticipant(int participantId);

    public List getAllContestsDetailed(User user);
    List getAllContestsDetailedFilter(Optional<String> contest_title, Optional<Integer> user_id,
                                      Optional<Integer> phase_id, Optional<String> sort, Optional<String> contests_judge, HttpSession httpSession);
    List<User> getContestOrganizers();
}
