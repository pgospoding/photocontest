package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.Comment;

import java.util.List;

public interface CommentRepository extends BaseCRUDRepository<Comment> {
    void DeleteAllByContestID (int id);
    List<Comment> getbySubmissionID(int id);
}
