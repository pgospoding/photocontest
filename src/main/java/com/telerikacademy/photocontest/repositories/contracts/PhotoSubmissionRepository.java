package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.models.PhotoSubmission;

import java.util.List;
import java.util.Optional;

public interface PhotoSubmissionRepository extends BaseCRUDRepository<PhotoSubmission> {
    List<PhotoSubmission> getAllByContestID(int id);
    List<PhotoSubmission> searchByTitle(Optional<String> filter);
    void DeleteByContestID (int id);
    List<PhotoSubmission> getTop10PhotoSubmissionsByContestID(int contest_id);
    List<PhotoSubmission> getByUserID(int id);

    PhotoSubmission getByContestId(int id, int contestId);
    List<PhotoSubmission> getByContestIdandUser(int id, int contestId);
}
