package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.Comment;
import com.telerikacademy.photocontest.models.Score;

public interface ScoreRepository extends BaseCRUDRepository<Score> {
   long getTotalByPhotoId(int photo_id);
   void DeleteAllByContestID (int id);
   Boolean getScoreByUserAndSubmission(int user_id, int submission_id);
}
