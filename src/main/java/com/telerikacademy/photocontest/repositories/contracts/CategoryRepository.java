package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.models.Contest;

public interface CategoryRepository extends BaseCRUDRepository<CategoryType> {
}
