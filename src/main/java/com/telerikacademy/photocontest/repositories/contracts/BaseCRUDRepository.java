package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.repositories.contracts.BaseCreateDeleteRepository;

public interface BaseCRUDRepository<T> extends BaseCreateDeleteRepository<T> {
    T update(T entity);
}
