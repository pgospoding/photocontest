package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends BaseCRUDRepository<User> {
    List<User> search(String search);

    List<User> getActiveWithRoleIdAndBadgeAtLeast(int roleId, int minIdValue);

    List<User> getActiveWithRoleAtLeast(int minRoleId);

    User emailIsEmailOfOtherUser(String email, int id);

    long getNumberOfUsers();

    public User findByResetPasswordToken(String token);

    @Query("SELECT c FROM User c WHERE c.email = ?1")
    public User findByEmail(String email);

//    TODO delete? AV ok
//    boolean isOrganizer(User user);
//    TODO delete? AV ok
//    List<User> getAlls();

}
