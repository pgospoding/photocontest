package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.Status;

public interface StatusRepository extends BaseReadRepository<Status> {

}
