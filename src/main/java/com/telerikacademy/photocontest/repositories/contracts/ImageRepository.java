package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.Image;

import java.util.List;

public interface ImageRepository extends BaseCRUDRepository<Image> {
    Image getImageById(int id);
    Image getImageByUserId(int user_id);
    Image createImage(Image image);
    String deleteImage(int id);
    List<Image> getAdultImages();
}
