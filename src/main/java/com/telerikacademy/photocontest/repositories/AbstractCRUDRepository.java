package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.repositories.contracts.BaseCRUDRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractCRUDRepository<T> extends AbstractCreateDeleteRepository<T>
        implements BaseCRUDRepository<T> {

    public AbstractCRUDRepository(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }

    @Override
    public T update(T entity) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
            return entity;
        }
    }
}
