package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository

public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }


    public List<User> search(String search) {
        try (Session session = sessionFactory.openSession()) {
            String searchString = "%" + search + "%";
            Query<User> query = session.createQuery("from User where username like :search or " +
                    "email like :search or first_name like :search or last_name like :search", User.class);

            query.setParameter("search", searchString);
            if (query.list().isEmpty()) {
                throw new ObjectNotFoundException("User", "whose username, name or email contain", search);
            }
            return query.list();
        }
    }

    //TODO check that methods go via AbstractRepository and delete
    public User getByField(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> userQuery = session.createQuery("from User where username = :username");
            userQuery.setParameter("username", username);
            List<User> users = userQuery.list();
            if (users.size() == 0) {
                throw new ObjectNotFoundException("User", "username", username);
            }
            return users.get(0);
        }
    }

    @Override
    public List<User> getActiveWithRoleIdAndBadgeAtLeast(int roleId, int minBadgeId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where badgeType.badge_id >= :badge and roleType.role_id = :role " +
                    "and status.status_id = 1 order by totalPoints DESC ");
            query.setParameter("badge", minBadgeId);
            query.setParameter("role", roleId);
            return query.list();
        }
    }

    @Override
    public List<User> getActiveWithRoleAtLeast(int minRoleId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where roleType.role_id >= :role " +
                    "and status.status_id = 1");
            query.setParameter("role", minRoleId);
            return query.list();
        }
    }

    public User emailIsEmailOfOtherUser(String email, int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where user_id != :user_id and email =:email");
            query.setParameter("email", email);
            query.setParameter("user_id", id);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new ObjectNotFoundException("User", "email", email);
            }

            return users.get(0);

        }
    }


    public User getUserByEmailHelper(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email =:email");
            query.setParameter("email", email);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new ObjectNotFoundException("User", "email", email);
            }

            return users.get(0);

        }
    }


    @Override
    public long getNumberOfUsers() {

        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("select count( distinct u.user_id ) from User u ", Long.class).getSingleResult();
        }
    }

    @Override
    public User findByResetPasswordToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where resetPasswordToken = :token");
            query.setParameter("token",token);
            User user = query.list().get(0);
            return user;
        }
    }

    @Override
    public User findByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email");
            query.setParameter("email",email);
            User user = query.list().get(0);
            return user;
        }
    }

}