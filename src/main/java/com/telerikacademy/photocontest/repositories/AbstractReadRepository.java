package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.repositories.contracts.BaseReadRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

import static java.lang.String.format;

public abstract class AbstractReadRepository<T> implements BaseReadRepository<T> {
    protected final Class<T> clazz;
    protected final SessionFactory sessionFactory;

    public AbstractReadRepository(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    public <V> T getUniqueByField(String name, V value) {
        try {
            return getListByField(name, value)
                    .stream()
                    .findAny().get();
//            TODO ask what is a good practice to emphasize that my method calls a method which throws exception?
        } catch (ObjectNotFoundException e){
            throw new ObjectNotFoundException((e.getMessage()));
        }

    }


    public <V> List<T> getListByField(String name, V value) {
        final String query = String.format("from %s where %s = :value", clazz.getName(), name);

        try (Session session = sessionFactory.openSession()) {
            List<T> itemList = session
                    .createQuery(query, clazz)
                    .setParameter("value", value)
                    .list();
            if (itemList.size() == 0) {
                throw new ObjectNotFoundException(clazz.getSimpleName(), name, String.valueOf(value));
            }
            return itemList;
        }
    }

    @Override
    public T getById(int id) {
        return getUniqueByField("id", id);
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(format("from %s ", clazz.getName()), clazz).list();
        }
    }
}
