package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Comment;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.repositories.contracts.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepositoryImpl extends AbstractCRUDRepository<Comment> implements CommentRepository {

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(Comment.class, sessionFactory);
    }

    @Override
    public void DeleteAllByContestID(int id) {
        List<Comment> result;
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where photoSubmission.photo_submission_id = :photo_sub_id"
                    , Comment.class);
            query.setParameter("photo_sub_id", id);
            result = query.list();
        }
        for(Comment comment : result){
            try(Session session2 = sessionFactory.openSession()){
                session2.beginTransaction();
                session2.delete(comment);
                session2.getTransaction().commit();
            }
        }
    }

    @Override
    public List<Comment> getbySubmissionID(int id){
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("from Comment where photoSubmission.photo_submission_id = :photo_sub_id"
                    , Comment.class);
            query.setParameter("photo_sub_id", id);
            List<Comment> result = query.list();
            return result;
        }
    }
}

