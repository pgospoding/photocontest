package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.PhaseTypeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhaseTypeRepositoryImpl extends AbstractCRUDRepository<PhaseType> implements PhaseTypeRepository {
    public PhaseTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(PhaseType.class, sessionFactory);
    }

}
