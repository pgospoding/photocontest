package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestWithPhase;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;
import com.telerikacademy.photocontest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(Contest.class, sessionFactory);
    }

    public Contest getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where title = :title", Contest.class);
            query.setParameter("title", title);
            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new ObjectNotFoundException("Contest", "title", title);
            }

            return result.get(0);
        }
    }

    public List<Contest> getByOrganizer(User user) {
        int user_id = user.getUser_id();
//        TODO delete commented part? AV OK
//        try (Session session = sessionFactory.openSession()) {
//            String sql = String.format("SELECT * from contests WHERE organizer=%d",user_id);
//            Query query = session.createSQLQuery(sql);
//            List results = query.list();
//            return results;
//        }
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where organizerId.id = :organizerId",
                    Contest.class);
            query.setParameter("organizerId", user_id);
            List<Contest> result = query.list();
            return result;
        }
    }

    @Override
    public List<Contest> filter(Optional<String> title,
                                Optional<Integer> categoryId,
                                Optional<Integer> phaseId,
                                Optional<ContestSortOptions> sort) {
        var listWithPhases = filterWithPhases(title, categoryId, phaseId, sort);
        return listWithPhases
                .stream()
                .map(contestWithPhase -> contestWithPhase.getContest_id())
                .map(id -> getById(id))
                .collect(Collectors.toList());
    }

    @Override
    public List<ContestWithPhase> filterWithPhases(Optional<String> title,
                                                   Optional<Integer> categoryId,
                                                   Optional<Integer> phaseId,
                                                   Optional<ContestSortOptions> sort) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("Select NEW com.telerikacademy.photocontest.models.ContestWithPhase(co.contest_id, " +
                    "co.title, co.categoryType, co.creationTime, co.is_finished, co.organizerId, co.photo, cp.phaseType) " +
                    " from ContestPhase cp left join Contest co on " +
                    "cp.contest = co.contest_id ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            LocalDateTime now = LocalDateTime.now();
            filters.add(" cp.start_time <= :localDateTime ");
            params.put("localDateTime", now);
            filters.add(" cp.end_time >= :localDateTime2 ");
            params.put("localDateTime2", now);

            title.ifPresent(value -> {
                filters.add(" co.title like :title");
                params.put("title", "%" + value + "%");
            });

            categoryId.ifPresent(value -> {
                filters.add(" co.categoryType.category_type_id = :categoryId ");
                params.put("categoryId", value);
            });

            phaseId.ifPresent(value -> {
                filters.add(" cp.phaseType.phase_type_id = :phaseId ");
                params.put("phaseId", value);
            });


            if (!filters.isEmpty()) {
                baseQuery.append(" where ").append(String.join(" and ", filters));
            }

            sort.ifPresent(value -> baseQuery.append(value.getQuery()));

            return session.createQuery(baseQuery.toString(), ContestWithPhase.class)
                    .setProperties(params)
                    .list();

        }
    }

    @Override
    public List getFinishedContestsByParticipant(int participantId) {
        return contestsByParticipantIdAndIsFinished(participantId, true);
    }

    @Override
    public List getRunningContestsByParticipant(int participantId) {
        return contestsByParticipantIdAndIsFinished(participantId, false);
    }

    List contestsByParticipantIdAndIsFinished(int participantId, boolean isFinished) {
        try (Session session = sessionFactory.openSession()) {
            String sql = String.format("SELECT contests.* from contests join contest_participants as p on contests.contest_id = p.contest_id " +
                    "where p.user_id = :participant_id and contests.is_finished =:is_finished");
            Query query = session.createNativeQuery(sql);
            query.setParameter("participant_id", participantId);
            query.setParameter("is_finished", isFinished);
            List results = query.list();
            return results;
        }
    }

    /*
        @Override
        TODO Delete filterAllPostsCustom method after more relevant method is created based on this one.
    */
    public List filterAllPostsCustom(int search_id, Optional<String> post_title, Optional<Integer> user_id, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            String sql = String.format("SELECT p.post_id, p.post_title, p.post_content, u.username," +
                    " (SELECT COUNT(*) FROM reactions_to_posts r WHERE r.post_id = p.post_id AND r.reaction_type=1) AS likesCount ," +
                    " (SELECT COUNT(*) FROM reactions_to_posts r WHERE r.post_id = p.post_id AND r.reaction_type=2) AS dislikesCount " +
                    ", i.image_link , (SELECT COUNT(*) FROM comments c WHERE c.post_id = p.post_id) AS commentsCount , " +
                    "(SELECT COUNT(*) FROM reactions_to_posts r WHERE r.post_id = p.post_id AND r.reaction_type=2 " +
                    "AND r.user_id=%s) AS dislikeUser ," +
                    " (SELECT COUNT(*) FROM reactions_to_posts r WHERE r.post_id = p.post_id AND r.reaction_type=1 " +
                    "AND r.user_id=%s) AS likeUser From Posts p Left JOIN images i on i.user_id = p.user_id Left " +
                    "JOIN users u on u.user_id = p.user_id", search_id, search_id);
            var baseQuery = new StringBuilder(sql);
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            post_title.ifPresent(value -> {
                filters.add(" p.post_title like :post_title");
                params.put("post_title", "%" + value + "%");
            });

            user_id.ifPresent(value -> {
                filters.add(" u.user_id = :user_id");
                params.put("user_id", value);
            });

            if (!filters.isEmpty()) {
                baseQuery.append(" where ").append(String.join(" and ", filters));
            }
            System.out.print(baseQuery);

            sort.ifPresent(value -> baseQuery.append(value));

            Query query = session.createSQLQuery(baseQuery.toString()).setProperties(params);
            List results = query.list();
            return results;

        }
    }

    //    TODO This is not used. Delete?
    public List<Contest> getIsFinishedFalse() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where is_finished = FALSE ", Contest.class);
            List<Contest> result = query.list();
            return result;
        }
    }

    @Override
    public List getAllContestsDetailed(User user) {
        try (Session session = sessionFactory.openSession()) {
//            String sql = String.format("SELECT c.contest_id,c.title,c.creation_time, cp.phase_id,cp.start_time,cp.end_time,\n" +
//                    "u.username,user_id,pt.phase_type,i.image_link,bt.badge_type,ct.category_type,p.user_id\n" +
//                    "FROM contests c LEFT JOIN contest_phases cp on c.contest_id = cp.cont_id\n" +
//                    "LEFT OUTER JOIN users u on c.organizer = u.user_id\n" +
//                    "LEFT OUTER JOIN phase_type pt on cp.phase_id = pt.phase_type_id\n" +
//                    "LEFT OUTER JOIN images i on c.cover_photo = i.image_id\n" +
//                    "LEFT OUTER JOIN badge_type bt on u.badge_id = bt.badge_type_id\n" +
//                    "LEFT OUTER JOIN category_type ct on c.category_id = ct.category_type_id\n" +
//                    "LEFT OUTER JOIN contest_participants p on c.contest_id = p.contest_id AND p.user_id = %s\n" +
//                    "WHERE ( (now() between start_time and end_time\n" +
//                    "   or (cp.phase_id=3 and now()>cp.end_time)))"
//            ,user.getUser_id());
            Integer logged_user_id = user.getUser_id();
            String sql = String.format("SELECT c.contest_id,c.title,c.creation_time, cp.phase_id,cp.start_time,cp.end_time,\n" +
                            "u.username,u.user_id as organizer_id,pt.phase_type,i.image_link,bt.badge_type,ct.category_type, p.user_id AS participant, j.user_id AS juror\n" +
                            "FROM contests c LEFT JOIN contest_phases cp on c.contest_id = cp.cont_id\n" +
                            "LEFT OUTER JOIN users u on c.organizer = u.user_id\n" +
                            "LEFT OUTER JOIN phase_type pt on cp.phase_id = pt.phase_type_id\n" +
                            "LEFT OUTER JOIN images i on c.cover_photo = i.image_id\n" +
                            "LEFT OUTER JOIN badge_type bt on u.badge_id = bt.badge_type_id\n" +
                            "LEFT OUTER JOIN category_type ct on c.category_id = ct.category_type_id\n" +
                            "LEFT OUTER JOIN contest_participants p on c.contest_id = p.contest_id AND p.user_id = %s\n" +
                            "LEFT OUTER JOIN juries j on c.contest_id = j.contest_id AND j.user_id = %s\n" +
                            "WHERE ( (now() between start_time and end_time\n" +
                            "   or (cp.phase_id=3 and now()>cp.end_time))) "
                    , logged_user_id, logged_user_id);
            Query query = session.createSQLQuery(sql);
            List results = query.list();
            return results;
        }
    }

    @Override
    public List getAllContestsDetailedFilter(Optional<String> contest_title, Optional<Integer> user_id,
                                             Optional<Integer> phase_id, Optional<String> sort,
                                             Optional<String> contests_judge,
                                             HttpSession httpSession) {
        Integer activeUser = (Integer) httpSession.getAttribute("user_id");
        System.out.print(activeUser);
        try (Session session = sessionFactory.openSession()) {
            String sql = String.format("SELECT c.contest_id,c.title,c.creation_time, cp.phase_id,cp.start_time,cp.end_time,\n" +
                            "u.username,u.user_id as organizer_id,pt.phase_type,i.image_link,bt.badge_type,ct.category_type, p.user_id AS participant, j.user_id AS juror\n" +
                            "FROM contests c LEFT JOIN contest_phases cp on c.contest_id = cp.cont_id\n" +
                            "LEFT OUTER JOIN users u on c.organizer = u.user_id\n" +
                            "LEFT OUTER JOIN phase_type pt on cp.phase_id = pt.phase_type_id\n" +
                            "LEFT OUTER JOIN images i on c.cover_photo = i.image_id\n" +
                            "LEFT OUTER JOIN badge_type bt on u.badge_id = bt.badge_type_id\n" +
                            "LEFT OUTER JOIN category_type ct on c.category_id = ct.category_type_id\n" +
                            "LEFT OUTER JOIN contest_participants p on c.contest_id = p.contest_id AND p.user_id = %s\n" +
                            "LEFT OUTER JOIN juries j on c.contest_id = j.contest_id AND j.user_id = %s\n" +
                            "WHERE ( (now() between start_time and end_time\n" +
                            "   or (cp.phase_id=3 and now()>cp.end_time)))"
                    , activeUser, activeUser);
            var baseQuery = new StringBuilder(sql);
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            contest_title.ifPresent(value -> {
                if(value.length()!=0){
                    filters.add(" c.title like :contest_title");
                    params.put("contest_title", "%" + value + "%");
                }
            });

            contests_judge.ifPresent(value -> {
                if(value.length()!=0) {
                    filters.add(" j.user_id = :initiatorId");
                    params.put("initiatorId", activeUser);
                }
            });

            user_id.ifPresent(value -> {
                filters.add(" u.user_id = :user_id");
                params.put("user_id", value);
            });

            phase_id.ifPresent(value -> {
                filters.add(" cp.phase_id = :phase_id");
                params.put("phase_id", value);
            });

            if (!filters.isEmpty()) {
                baseQuery.append(" and ").append(String.join(" and ", filters));
            }

            sort.ifPresent(value -> baseQuery.append(value));
            System.out.printf(baseQuery.toString());
            Query query = session.createSQLQuery(baseQuery.toString()).setProperties(params);
            List results = query.list();
            return results;

        }

    }

    @Override
    public List<User> getContestOrganizers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("Select distinct organizerId from Contest ", User.class);
            return query.list();
        }
    }
}
