package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestPhase;
import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.repositories.contracts.ContestPhaseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ContestPhaseRepositoryImpl extends AbstractCRUDRepository<ContestPhase> implements ContestPhaseRepository {

    @Autowired
    public ContestPhaseRepositoryImpl(SessionFactory sessionFactory) {
        super(ContestPhase.class, sessionFactory);
    }

//    TODO test and, if not works, replace with method returning ContestWithPhase
    @Override
    public List<Contest> getAllByPhase(int phase_type_id) {
        try (Session session = sessionFactory.openSession()) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Query<Contest> query = session.createQuery("from Contest where id in (SELECT contest.id from ContestPhase where phaseType.phase_type_id = :phase_type_id and start_time >= :localDatetime and end_time <:localDatetime2 )", Contest.class);
            query.setParameter("phase_type_id", phase_type_id);
            query.setParameter("localDatetime", localDateTime);
            query.setParameter("localDatetime2", localDateTime);
            List<Contest> result = query.list();
            return result;
        }
    }

//    TODO delete? Same expected output as the above
    @Override
    public List<Contest> getContestsByPhase(int phaseTypeId) {
        try (Session session = sessionFactory.openSession()) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Query<Contest> query = session.createQuery("Select contest from ContestPhase cp where start_time " +
                    "< :localDatetime and end_time >= :localDatetime2 and cp.phaseType.phase_type_id = :phaseId");
            query.setParameter("localDatetime", localDateTime);
            query.setParameter("localDatetime2", localDateTime);
            query.setParameter("phaseId",phaseTypeId);
            return query.list();
        }
    }



//    TODO Georgi, delete?
//    @Override
//    public List getAllUnfinished() {
//        try (Session session = sessionFactory.openSession()) {
//            LocalDateTime localDateTime = LocalDateTime.now();
//            Query<Contest> query = session.createQuery("from ContestPhase cp Left JOIN Contest c on c.id = cp.contest.id " +
//                    "where cp.phaseType = '3' and cp.end_time <= :localDatetime and c.is_finished = FALSE ", Contest.class);
//            query.setParameter("localDatetime", localDateTime);
//            List result = query.list();
//            System.out.print(result.get(0).toString());
//            return result;
//        }
//    }
    @Override
    public List<Integer> getAllToBeMarkedAsFinished(){
        try (Session session = sessionFactory.openSession()) {
            LocalDateTime localDateTime = LocalDateTime.now();
//            TODO choose correct implementation between Sasha and Georgi. Georgi's original commented
//            String sql = String.format("SELECT c.contest_id from contest_phases cp Left JOIN contests c " +
//                    "on c.contest_id = cp.cont_id where cp.phase_id = '3' and CAST(cp.end_time as timestamp) <= now() " +
//                    "and c.is_finished = 'false'");
            String sql = String.format("SELECT c.contest_id from contest_phases cp Left JOIN contests c " +
                    "on c.contest_id = cp.cont_id where cp.phase_id = '3' and CAST(cp.start_time as timestamp) <= now() " +
                    "and c.is_finished = 'false'");
            Query query = session.createSQLQuery(sql);
            List result = query.list();
            return result;
        }
    }

    @Override
    public List<ContestPhase> getAllPhasesByContestID(int id){
        try (Session session = sessionFactory.openSession()) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Query<ContestPhase> query = session.createQuery("from ContestPhase where contest.id  = :contest_id");
            query.setParameter("contest_id",id);
            return query.list();
        }
    }

    @Override
    public ContestPhase getPhaseByPhaseNumberAndContestID(int phaseId, int contestId){
        try (Session session = sessionFactory.openSession()) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Query<ContestPhase> query = session.createQuery("from ContestPhase where contest.id  = :contest_id" +
                    " and phaseType.phase_type_id = :phase_id");
            query.setParameter("contest_id",contestId);
            query.setParameter("phase_id", phaseId);
            List<ContestPhase> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException();
            }

            return result.get(0);
        }
    }

    @Override
    public List<PhaseType> getPhaseTypes() {
        try (Session session = sessionFactory.openSession()) {
            Query<PhaseType> query = session.createQuery("Select distinct phaseType from ContestPhase ",PhaseType.class);
            return query.list();
        }
    }

    @Override
    public Integer getContestPhase(int contestID) {
        try (Session session = sessionFactory.openSession()) {
            LocalDateTime localDateTime = LocalDateTime.now();
            Query<ContestPhase> query = session.createQuery("from ContestPhase cp where start_time " +
                    "< :localDatetime and end_time >= :localDatetime2 and cp.contest.id = :contestID");
            query.setParameter("localDatetime", localDateTime);
            query.setParameter("localDatetime2", localDateTime);
            query.setParameter("contestID", contestID);
            return query.list().get(0).getPhaseType().getPhase_type_id();
        }
    }
}
