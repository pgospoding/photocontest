package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.Status;
import com.telerikacademy.photocontest.repositories.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class StatusRepositoryImpl extends AbstractReadRepository<Status> implements StatusRepository {
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(Status.class, sessionFactory);
    }
}
