package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.BadgeType;
import com.telerikacademy.photocontest.models.RoleType;
import com.telerikacademy.photocontest.models.Status;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.DTOs.UserCreateDTO;
import org.springframework.stereotype.Component;

@Component
public class UserCreateMapper {

    public User dtoToObject(UserCreateDTO userCreateDTO){
    User user = new User();
    user.setFirst_name(userCreateDTO.getFirst_name());
    user.setLast_name(userCreateDTO.getLast_name());
    user.setEmail(userCreateDTO.getEmail());
    user.setUsername(userCreateDTO.getUsername());
    user.setPassword(userCreateDTO.getPassword());
    user.setRoleType(RoleType.USER);
    user.setTotalPoints(0);
    user.setBadgeType(BadgeType.JUNKIE);
    user.setStatus(Status.ACTIVE);
    return user;
    }

}
