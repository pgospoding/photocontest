package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.DTOs.ScoreDTO;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.Score;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.ScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScoreMapper {
    private final ScoreRepository repository;

    @Autowired
    public ScoreMapper(ScoreRepository repository) {
        this.repository = repository;
    }
    public Score dtoToObject(ScoreDTO scoreDTO){
        Score score = new Score();
        score.setScore(scoreDTO.getScore());

        return score;
    }

    public Score dtoToObject(ScoreDTO scoreDTO, User user, PhotoSubmission photoSubmission) {
        Score score = new Score();
        score.setScore(scoreDTO.getScore());
        score.setPhotoSubmission(photoSubmission);
        score.setUser(user);
        return score;
    }

}
