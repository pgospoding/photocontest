package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.DTOs.SelfUpdDTO;
import org.springframework.stereotype.Component;

@Component
public class SelfUpdateMapper {


    public SelfUpdateMapper() {
    }

    public User dtoToObject(SelfUpdDTO selfUpdDTO, User initiator){
        User user = new User();
        //if a DTO field is null, use fields of user who initiated action
        user.setUser_id(initiator.getUser_id());
       trySetFirstName(user, selfUpdDTO, initiator);
       trySetLastName(user, selfUpdDTO, initiator);
       trySetEmail(user, selfUpdDTO, initiator);
       user.setUsername((initiator.getUsername()));
       user.setPassword(initiator.getPassword());
        user.setRoleType(initiator.getRoleType());
        user.setTotalPoints(initiator.getTotalPoints());
        user.setBadgeType();
        user.setStatus(initiator.getStatus());
       return user;
    }

    public SelfUpdDTO objectToDTO(User initiator){
        SelfUpdDTO userDTO = new SelfUpdDTO();
        userDTO.setFirst_name(initiator.getFirst_name());
        userDTO.setLast_name(initiator.getLast_name());
        userDTO.setEmail(initiator.getEmail());
        return userDTO;
    }

    public void trySetFirstName(User user, SelfUpdDTO userDTOOwnInfoUpd, User initiator){
        if (userDTOOwnInfoUpd.getFirst_name()==null){
            user.setFirst_name(initiator.getFirst_name());
        }
        else {
            user.setFirst_name(userDTOOwnInfoUpd.getFirst_name());
        }
    }

    public void trySetLastName(User user, SelfUpdDTO userDTOOwnInfoUpd, User initiator){
        if (userDTOOwnInfoUpd.getLast_name()==null){
            user.setLast_name(initiator.getLast_name());
        }
        else {
            user.setLast_name(userDTOOwnInfoUpd.getLast_name());
        }
    }

    public void trySetEmail(User user, SelfUpdDTO userDTOOwnInfoUpd, User initiator){
        if (userDTOOwnInfoUpd.getEmail()==null){
            user.setEmail(initiator.getEmail());
        }
        else {
            user.setEmail(userDTOOwnInfoUpd.getEmail());
        }
    }

}
