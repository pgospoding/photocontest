package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.Comment;
import com.telerikacademy.photocontest.models.DTOs.CommentDTO;
import com.telerikacademy.photocontest.models.DTOs.CommentUpdateDTO;
import com.telerikacademy.photocontest.repositories.contracts.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommentUpdateMapper {
    private final ContestRepository contestRepository;
    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;
    private final CommentRepository commentRepository;
    private final PhotoSubmissionRepository photoSubmissionRepository;
    private final UserRepository userRepository;

    public CommentUpdateMapper(ContestRepository contestRepository, CategoryRepository categoryRepository, ImageRepository imageRepository, CommentRepository commentRepository, PhotoSubmissionRepository photoSubmissionRepository, UserRepository userRepository) {
        this.contestRepository = contestRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.commentRepository = commentRepository;
        this.photoSubmissionRepository = photoSubmissionRepository;
        this.userRepository = userRepository;
    }

    public Comment dtoToObject(CommentUpdateDTO commentUpdateDTO, int id){
        Comment comment = new Comment();
        comment.setContent(commentUpdateDTO.getContent());
        comment.setPhotoSubmission(commentRepository.getById(id).getPhotoSubmission());
        comment.setUser(commentRepository.getById(id).getUser());
        comment.setCreation_time(commentRepository.getById(id).getCreation_time());
        return comment ;
    }

    public CommentUpdateDTO objectToDTO(Comment comment){
        CommentUpdateDTO commentUpdateDTO = new CommentUpdateDTO();
        commentUpdateDTO.setContent(comment.getContent());
        return commentUpdateDTO;
    }
}
