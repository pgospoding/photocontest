package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.DTOs.MVCUserDTO_byIdUpd;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class MVCUserByIdUpdMapper {

    UserRepository userRepository;

    public MVCUserByIdUpdMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User dtoToObject(MVCUserDTO_byIdUpd userByIdUpdDTO) {
        User oldUserInfo;
        try {
            oldUserInfo = userRepository.getById(userByIdUpdDTO.getUser_id());
        } catch (ObjectNotFoundException e) {
            throw new ObjectNotFoundException(e.getMessage());
        }
        User user = new User();
        //if a DTO field is null, use fields from old user info
        user.setUser_id(userByIdUpdDTO.getUser_id());
        user.setFirst_name(userByIdUpdDTO.getFirst_name());
        user.setLast_name(userByIdUpdDTO.getLast_name());
        user.setUsername(userByIdUpdDTO.getUsername());
        user.setEmail(userByIdUpdDTO.getEmail());
        user.setPassword(oldUserInfo.getPassword());
        user.setRoleType(userByIdUpdDTO.getRoleType());
        user.setTotalPoints(oldUserInfo.getTotalPoints());
        user.setBadgeType();
        user.setStatus(userByIdUpdDTO.getStatus());
        return user;
    }

    public MVCUserDTO_byIdUpd objectToDTO (int user_id){
        User oldUserInfo;
        try {
            oldUserInfo = userRepository.getById(user_id);
        } catch (ObjectNotFoundException e) {
            throw new ObjectNotFoundException(e.getMessage());
        }
        MVCUserDTO_byIdUpd userDTO = new MVCUserDTO_byIdUpd();
        userDTO.setUser_id(oldUserInfo.getUser_id());
        userDTO.setFirst_name(oldUserInfo.getFirst_name());
        userDTO.setLast_name(oldUserInfo.getLast_name());
        userDTO.setUsername(oldUserInfo.getUsername());
        userDTO.setEmail(oldUserInfo.getEmail());
        userDTO.setRoleType(oldUserInfo.getRoleType());
        userDTO.setStatus(oldUserInfo.getStatus());
        return userDTO;
    }

}
