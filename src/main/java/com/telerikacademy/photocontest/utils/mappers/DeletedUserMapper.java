package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.Status;
import com.telerikacademy.photocontest.models.User;
import org.springframework.stereotype.Component;

@Component
public class DeletedUserMapper {
    public static final String DELETED_NAME = "User";
    public static final String DELETED_SURNAME = "Deleted";
    public static final String DELETED_USERNAME_ADDITION = " (deleted)";
    public static final String DELETED_EMAIL_ADDITION = "@noemail";
    public static final String DELETED_PASSWORD = "passwordDeleted";
    public DeletedUserMapper() {
    }

    public User userToDeletedUser (User userToBeDeleted) {
        User deletedUserInfo = new User();
        deletedUserInfo.setUser_id(userToBeDeleted.getUser_id());
        deletedUserInfo.setUsername(userToBeDeleted.getUsername()+DELETED_USERNAME_ADDITION);
        deletedUserInfo.setFirst_name(DELETED_NAME);
        deletedUserInfo.setLast_name(DELETED_SURNAME);
        deletedUserInfo.setEmail(userToBeDeleted.getUsername()+DELETED_EMAIL_ADDITION);
        deletedUserInfo.setPassword(DELETED_PASSWORD);
        deletedUserInfo.setRoleType(userToBeDeleted.getRoleType());
        deletedUserInfo.setTotalPoints(userToBeDeleted.getTotalPoints());
        deletedUserInfo.setBadgeType();
        deletedUserInfo.setStatus(Status.DELETED);
        return deletedUserInfo;
    }
}
