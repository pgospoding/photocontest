package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.DTOs.ContestDTO;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.CategoryRepository;
import com.telerikacademy.photocontest.repositories.contracts.ContestRepository;
import com.telerikacademy.photocontest.repositories.contracts.ImageRepository;
import com.telerikacademy.photocontest.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class ContestMapper {
    private final ContestRepository contestRepository;
    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;
    private final UserRepository userRepository;


    public ContestMapper(ContestRepository contestRepository, CategoryRepository categoryRepository, ImageRepository imageRepository, UserRepository userRepository) {
        this.contestRepository = contestRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.userRepository = userRepository;
    }

    public Contest dtoToObject(ContestDTO contestDTO, int id){
        Contest contest = contestRepository.getById(id);
        contest.setTitle(contestDTO.getTitle());
        contest.setCreation_time(LocalDateTime.now());

        CategoryType category = categoryRepository.getById(contestDTO.getCategoryId());
        contest.setCategoryType(category);

        contest.setIs_finished(Boolean.FALSE);

        return contest;
    }

    public Contest dtoToObject(ContestDTO contestDTO, User user){
        Contest contest = new Contest();
        contest.setTitle(contestDTO.getTitle());
        contest.setCreation_time(LocalDateTime.now());

        CategoryType category = categoryRepository.getById(contestDTO.getCategoryId());
        contest.setCategoryType(category);


        contest.setOrganizer_id(user);
        Set<User> organizersAndAdmins = new HashSet<>();
        organizersAndAdmins.addAll(userRepository.getActiveWithRoleAtLeast(2));
        contest.setJurors(organizersAndAdmins);
        contest.setIs_finished(Boolean.FALSE);

        return contest;
    }

    private void dtoToObject(ContestDTO contestDTO, Contest contest) {
        CategoryType category = categoryRepository.getById(contestDTO.getCategoryId());

        contest.setTitle(contestDTO.getTitle());
        contest.setCategoryType(category);
        Set<User> organizersAndAdmins = new HashSet<>();
        organizersAndAdmins.addAll(userRepository.getActiveWithRoleAtLeast(2));
        contest.setJurors(organizersAndAdmins);
        contest.setIs_finished(Boolean.FALSE);

    }

    public ContestDTO toDto(Contest contest) {
        ContestDTO contestDTO = new ContestDTO();
        contestDTO.setTitle(contest.getTitle());
        contestDTO.setCategoryId(contest.getCategoryType().getCategory_type_id());
        return contestDTO;
    }

    public Contest fromDto(ContestDTO contestDTO, int id) {
        Contest contest = contestRepository.getById(id);
        dtoToObject(contestDTO, contest);
        return contest;
    }
}
