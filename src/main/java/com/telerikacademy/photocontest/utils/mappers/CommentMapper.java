package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.Comment;
import com.telerikacademy.photocontest.models.DTOs.CommentDTO;
import com.telerikacademy.photocontest.models.DTOs.PhotoSubmissionDTO;
import com.telerikacademy.photocontest.models.Image;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommentMapper {
    private final ContestRepository contestRepository;
    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;
    private final PhotoSubmissionRepository photoSubmissionRepository;
    private final UserRepository userRepository;

    public CommentMapper(ContestRepository contestRepository, CategoryRepository categoryRepository, ImageRepository imageRepository, PhotoSubmissionRepository photoSubmissionRepository, UserRepository userRepository) {
        this.contestRepository = contestRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.photoSubmissionRepository = photoSubmissionRepository;
        this.userRepository = userRepository;
    }

    public Comment dtoToObject(CommentDTO commentDTO, User user){
        Comment comment = new Comment();
        comment.setContent(commentDTO.getContent());
        comment.setPhotoSubmission(photoSubmissionRepository.getById(commentDTO.getPhotoSubmission_id()));
        comment.setUser(user);
        comment.setCreation_time(LocalDateTime.now());
        return comment ;
    }

    public CommentDTO objectToDTO(Comment comment){
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setContent(comment.getContent());
        commentDTO.setPhotoSubmission_id(comment.getPhotoSubmission().getPhoto_submission_id());
        return commentDTO;
    }
}
