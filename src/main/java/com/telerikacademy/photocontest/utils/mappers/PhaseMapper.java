package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.DTOs.PhaseDTO;
import com.telerikacademy.photocontest.repositories.contracts.ContestPhaseRepository;
import com.telerikacademy.photocontest.repositories.contracts.PhaseTypeRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class PhaseMapper {
    private final PhaseTypeRepository phaseTypeRepository;
    private final ContestPhaseRepository contestPhaseRepository;

    public PhaseMapper(PhaseTypeRepository phaseTypeRepository, ContestPhaseRepository contestPhaseRepository) {
        this.phaseTypeRepository = phaseTypeRepository;
        this.contestPhaseRepository = contestPhaseRepository;
    }


    public ContestPhase dtoToObject(PhaseDTO phaseDTO, Contest contest){
        ContestPhase phase = new ContestPhase();
        phase.setContest(contest);
        phase.setStart_time(phaseDTO.getStart_time());
        phase.setEnd_time(phaseDTO.getEnd_time());

        PhaseType phaseType = phaseTypeRepository.getById(phaseDTO.getPhaseType());

        return phase;
    }

    public ContestPhase dtoToObject(PhaseDTO phaseDTO, int id, Contest contest){
        ContestPhase contestPhase = contestPhaseRepository.getById(id);
        ContestPhase phase = new ContestPhase();
        phase.setContest(contest);
        phase.setStart_time(phaseDTO.getStart_time());
        phase.setEnd_time(phaseDTO.getEnd_time());

        PhaseType phaseType = phaseTypeRepository.getById(phaseDTO.getPhaseType());

        return contestPhase;
    }
}
