package com.telerikacademy.photocontest.exceptions;

public class DuplicateUsernameException extends DuplicateEntityException{
    public DuplicateUsernameException(String duplicate_username) {
        super("User", "username", duplicate_username);
    }
}
