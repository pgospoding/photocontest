package com.telerikacademy.photocontest.exceptions;

public class DuplicateEmailException extends DuplicateEntityException{
    public DuplicateEmailException(String duplicate_email) {
        super("User", "email", duplicate_email);
    }
}
