package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Comment;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.CommentRepository;
import com.telerikacademy.photocontest.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository repository;

    @Autowired
    public CommentServiceImpl(CommentRepository repository) {
        this.repository = repository;
    }

    @Override
    public Comment create(Comment comment) {
        return repository.create(comment);
    }

    @Override
    public Comment getComment(int id) {
        return repository.getById(id);
    }

    @Override
    public Comment update(Comment comment, User user) {
        if (!comment.getUser().getUsername().equals(user.getUsername()))
            throw new UnauthorizedOperationException("You are not the author of this item or an admin");
        return repository.update(comment);
    }

    @Override
    public String delete(int id, User user) {
        if (!repository.getById(id).getUser().getUsername().equals(user.getUsername()))
            throw new UnauthorizedOperationException("You are not the author of this item or an admin");
        return repository.delete(id);
    }

    @Override
    public void deleteByID(int id) {
        repository.delete(id);
    }

    @Override
    public void deleteAllByContestID(int id) {
        repository.DeleteAllByContestID(id);
    }

    @Override
    public List<Comment> getBySubmissionID(int id){
        return repository.getbySubmissionID(id);
    }
}
