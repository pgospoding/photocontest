package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.Comment;
import com.telerikacademy.photocontest.models.User;

import java.util.List;

public interface CommentService {

    Comment create(Comment comment);
    Comment getComment(int id);
    Comment update(Comment comment, User user);
    String delete(int id, User user);
    void deleteAllByContestID(int id);
    void deleteByID(int id);
    List<Comment> getBySubmissionID(int id);
}
