package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.Image;

import java.util.List;

public interface ImageService {
    Image getImageById (int id);
    Image getImageByUserId(int user_id);
    Image createImage(Image image);
    Image censorImage(Image image);
    String deleteImage(int id);
    List<Image> getAdultImages();
}
