package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.RoleType;

import java.util.List;

public interface RoleTypeService {
    List<RoleType> getAll();
}
