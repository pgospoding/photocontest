package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.Status;

import java.util.List;

public interface StatusService {
    List<Status> getAll();
}
