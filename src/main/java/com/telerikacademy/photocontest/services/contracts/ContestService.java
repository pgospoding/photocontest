package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestWithPhase;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

public interface ContestService {
    List<Contest> getAll(Optional<String> title, Optional<Integer> categoryType, Optional<Integer> phaseId,
                         Optional<ContestSortOptions> sort);

    List<ContestWithPhase> getAllWithPhases(Optional<String> title,
                                            Optional<Integer> categoryId,
                                            Optional<Integer> phaseId,
                                            Optional<ContestSortOptions> sort);

    Contest getById(int id);

    List<Contest> getByOrganizer(User user);

    List<Contest> getByPhaseId(int phaseId);

    void create(Contest contest);

    void update(Contest contest, User loggedUser);

    void delete(int id, User loggedUser);

    Contest addJurors(int contestId, User user, User logged);

    Contest removeJurors(int contestId, User user, User logged);

    Contest addParticipants(int contestId, User user);

    void updateFinishedContests();

    List<User> getContestOrganizers();

    public List getAllContestsDetailed(User user);

    //It doesn't throw exceptions if user not found. In that case, an empty list is returned.
    List getFinishedContestsByParticipant(int participantId);

    //It doesn't throw exceptions if user not found. In that case, an empty list is returned.
    List getRunningContestsByParticipant(int participantId);
//    TODO rename
    List filterAllPostsCustom(Optional<String> contest_title, Optional<Integer> user_id, Optional<Integer> phase_id, Optional<String> sort, Optional<String> contests_judge, HttpSession httpSession);

    boolean isParticipant(int contest_id, User user);

    boolean isJury(int contest_id, User user);

    List<PhotoSubmission> getWinningPhotos(int contestId);

    List<Contest> getContestToJudge(User user);
}
