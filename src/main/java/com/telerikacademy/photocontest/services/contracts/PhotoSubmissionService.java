package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.Score;
import com.telerikacademy.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface PhotoSubmissionService {

    void create(PhotoSubmission photoSubmission);

    PhotoSubmission getByID(int id);

    public List<PhotoSubmission> getPhotoSubmissionsByContestId(int id);

    String delete(int id, User loggeduser);

    List<PhotoSubmission> searchByTitle(Optional<String> title);

    PhotoSubmission setScore(int photoSubmissionId, Score score);

    PhotoSubmission setTotalScore(int photoSubmissionId);

    PhotoSubmission getByContestId(int id, int contestId);

    void deleteByContestID(int id);

}
