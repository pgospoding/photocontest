package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestPhase;
import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.models.User;

import java.util.List;

public interface ContestPhaseService {
    List<Contest> getAllByPhase(int phase_type_id);

    List<ContestPhase> getAll();

    ContestPhase getById(int id);

    void update(ContestPhase contestPhase, Contest contest, User loggedUser);

    void delete(int id, Contest contest, User loggedUser);
    public List<Integer> checkFinishedContests();

    public void create(ContestPhase contestPhase);
    public ContestPhase getByID(int id);
    void deleteByContestID(int id, User loggedUser);

    List<ContestPhase> getAllPhasesByContestID(int contestId);

    ContestPhase getPhaseOneByContestID(int contestId);

    ContestPhase getPhaseTwoByContestID(int contestId);

    ContestPhase getPhaseThreeByContestID(int contestId);

    List<PhaseType> getPhaseTypes();

    Integer getContestPhase(int contestID);
}
