package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.Score;
import com.telerikacademy.photocontest.models.User;

import java.util.List;

public interface ScoreService {
    List<Score> getAll();

    Score getById(int id);

    List<Score> getByPhotoId(int photo_id);

    Score create(Score score);

    Score update(Score score, User user);

    String delete(int id, User user);

    void DeleteAllByContestID(int id);

    Boolean getScoreByUserAndSubmission(int user_id, int submission_id);
}
