package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.*;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.repositories.contracts.*;
import com.telerikacademy.photocontest.services.contracts.UserService;
import com.telerikacademy.photocontest.utils.mappers.DeletedUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final ContestRepository contestRepository;
    private final ImageRepository imageRepository;
    private final PhotoSubmissionRepository photoSubmissionRepository;
    private final DeletedUserMapper deletedUserMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           CommentRepository commentRepository,
                           ContestRepository contestRepository,
                           ImageRepository imageRepository,
                           PhotoSubmissionRepository photoSubmissionRepository, DeletedUserMapper deletedUserMapper) {
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
        this.contestRepository = contestRepository;
        this.imageRepository = imageRepository;
        this.photoSubmissionRepository = photoSubmissionRepository;
        this.deletedUserMapper = deletedUserMapper;
    }

    public List<User> getAll(Optional<String> search, User initiator) {
        if ((initiator.getRoleType().equals(RoleType.ADMIN) ||
                initiator.getRoleType().equals(RoleType.ORGANIZER))
                && initiator.getStatus().equals(Status.ACTIVE)) {
            try {
                if (!search.isPresent()) {
                    return userRepository.getAll();
                } else {
                    return userRepository.search(search.get());
                }
            } catch (ObjectNotFoundException e) {
                throw new ObjectNotFoundException(e.getMessage());
            }
        } else throw new UnauthorizedOperationException("You don't have rights to view user list and search in it.");
    }


    public User getById(int id, User initiator) {
        if ((initiator.getRoleType().equals(RoleType.ADMIN) ||
                initiator.getRoleType().equals(RoleType.ORGANIZER) ||
                initiator.getUser_id() == id)
                && initiator.getStatus().equals(Status.ACTIVE)) {
            return userRepository.getById(id);
        } else throw new UnauthorizedOperationException("You don't have rights to view user information");
    }

    //helper method, no controller attached, therefore no identification check
    public User getByUsername(String username) {
        return userRepository.getUniqueByField("username", username);
    }

    public User create(User user) {
        boolean isDuplicate = true;
        try {
            userRepository.getUniqueByField("username", user.getUsername());
        } catch (ObjectNotFoundException e) {
            isDuplicate = false;
        }
        if (isDuplicate) {
            throw new DuplicateUsernameException(user.getUsername());
        }
        isDuplicate = true;
        try {
            userRepository.getUniqueByField("email", user.getEmail());
        } catch (ObjectNotFoundException e) {
            isDuplicate = false;
        }
        if (isDuplicate) {
            throw new DuplicateEmailException(user.getEmail());
        }
        return userRepository.create(user);
    }

    public User updateSelf(User newUserInfo) {
        if (!isEmailUnique(newUserInfo.getEmail(), newUserInfo.getUser_id())) {
            throw new DuplicateEntityException("User", "email", newUserInfo.getEmail());
        }
        userRepository.update(newUserInfo);
        return newUserInfo;
    }

    public User updateById(User initiator, User newUserInfo) {
        if (!initiator.isActiveAdmin()) {
            throw new UnauthorizedOperationException("You don't have rights to update information of other users.");
        }
        if (!isEmailUnique(newUserInfo.getEmail(), newUserInfo.getUser_id())) {
            throw new DuplicateEntityException("User", "email", newUserInfo.getEmail());
        }
        userRepository.update(newUserInfo);
        return userRepository.getById(newUserInfo.getUser_id());
    }

    //at deletion, we delete user first name, last name and email
    @Override
    public String delete(int id, User initiator) {

        if (!initiator.isActiveAdmin()) {
            throw new UnauthorizedOperationException("You don't have rights to delete users.");
        }
        String message = "";
        User userToBeDeleted;
        try {
            userToBeDeleted = userRepository.getById(id);
        } catch (ObjectNotFoundException e) {
            throw new ObjectNotFoundException("User", "id", String.valueOf(id));
        }
        User deletedUserInfo = deletedUserMapper.userToDeletedUser(userToBeDeleted);

        try {
            updateById(initiator, deletedUserInfo);
            //Object not found exception already thrown in getById
        } catch (DuplicateEntityException f) {
            throw new DuplicateEntityException(f.getMessage());
        }
        return "User information has been successfully deleted";
    }

    public boolean isEmailUnique(String email, int user_id) {
        try {
            userRepository.emailIsEmailOfOtherUser(email, user_id);
        } catch (ObjectNotFoundException e) {
            return true;
        }
        return false;
    }

    @Override
    public long getNumberOfUsers() {
        return userRepository.getNumberOfUsers();
    }

    @Override
    public List<User> getActiveWithRoleIdAndBadgeAtLeast(int roleId, int minBadgeId, User initiator) {
        if ((initiator.getRoleType().equals(RoleType.ADMIN) ||
                initiator.getRoleType().equals(RoleType.ORGANIZER))
                && initiator.getStatus().equals(Status.ACTIVE)) {
            return userRepository.getActiveWithRoleIdAndBadgeAtLeast(roleId, minBadgeId);
        } else throw new UnauthorizedOperationException("You don't have rights to view user list.");
    }

    @Override
    public List<User> getActiveWithRoleIdAtLeast(int minRoleId, User initiator) {
        if ((initiator.getRoleType().equals(RoleType.ADMIN) ||
                initiator.getRoleType().equals(RoleType.ORGANIZER))
                && initiator.getStatus().equals(Status.ACTIVE)) {
            return userRepository.getActiveWithRoleAtLeast(minRoleId);
        } else throw new UnauthorizedOperationException("You don't have rights to view user list.");
    }

    @Override
    public void updateResetPasswordToken(String token, String email) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.update(user);
        } else {
            throw new EntityNotFoundException("Could not find any user with this email ");
        }
    }

    public User getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    public void updatePassword(User user, String newPassword) {
        user.setPassword(newPassword);
        user.setResetPasswordToken(null);
        userRepository.update(user);
    }


}

