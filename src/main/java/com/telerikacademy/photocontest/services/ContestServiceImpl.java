package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;
import com.telerikacademy.photocontest.repositories.contracts.ContestPhaseRepository;
import com.telerikacademy.photocontest.repositories.contracts.ContestRepository;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.telerikacademy.photocontest.repositories.contracts.UserRepository;
import com.telerikacademy.photocontest.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ContestServiceImpl implements ContestService {
    public static final int[] NORMAL_PLACE_POINTS = {50, 35, 20};
    public static final int BONUS_FIRST_PLACE_POINTS_FOR_DOUBLE_SCORE = 25;
    public static final int[] SHARED_PLACE_POINTS = {40, 25, 10};
    private final ContestRepository contestRepository;
    private final PhotoSubmissionRepository photoSubmissionRepository;
    private final UserRepository userRepository;
    private final ContestPhaseRepository contestPhaseRepository;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository,
                              PhotoSubmissionRepository photoSubmissionRepository,
                              UserRepository userRepository,
                              ContestPhaseRepository contestPhaseRepository) {
        this.contestRepository = contestRepository;
        this.photoSubmissionRepository = photoSubmissionRepository;
        this.userRepository = userRepository;
        this.contestPhaseRepository = contestPhaseRepository;
    }

    @Override
    public List<Contest> getAll(Optional<String> title, Optional<Integer> categoryId, Optional<Integer> phaseId,
                                         Optional<ContestSortOptions> sort) {
        return contestRepository.filter(title, categoryId, phaseId, sort);
    }

    @Override
    public List<ContestWithPhase> getAllWithPhases(Optional<String> title, Optional<Integer> categoryId, Optional<Integer> phaseId,
                                                   Optional<ContestSortOptions> sort) {
        return contestRepository.filterWithPhases(title, categoryId, phaseId, sort);
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public List<Contest> getByOrganizer(User user) {
        return contestRepository.getByOrganizer(user);
    }

    @Override
    public List<Contest> getByPhaseId(int phaseId) {
        return contestPhaseRepository.getContestsByPhase(phaseId);
    }

    @Override
    public void create(Contest contest) {
        boolean duplicateExists = true;
        try{
            Contest contest_duplicate = contestRepository.getByTitle(contest.getTitle());
        } catch (ObjectNotFoundException e) {
            duplicateExists=false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        contestRepository.create(contest);
    }

    @Override
    public void update(Contest contest, User loggedUser) {

        if (!contest.getOrganizer_id().equals(loggedUser) && !loggedUser.isActiveAdmin()) {
            throw new UnauthorizedOperationException("Only organizer or admin can modify a contest.");
        }

        contestRepository.update(contest);
    }

    @Override
    public void delete(int id, User loggedUser) {
        Contest contest = contestRepository.getById(id);
        if (!contest.getOrganizer_id().equals(loggedUser) && !loggedUser.isActiveAdmin()) {
            throw new UnauthorizedOperationException("Only organizer or admin can modify a contest.");
        }
        contestRepository.delete(id);
    }

    @Override
    public Contest addJurors(int contestId, User user, User logged) {
        Contest contest = contestRepository.getById(contestId);
        if (logged == contest.getOrganizer_id()) {
            throw new UnauthorizedOperationException("Only the organizer of the contest is allowed to add jury.");
        }

        Set<User> jurors = contest.getJurors();
        jurors.add(user);
        contestRepository.update(contest);

        return contest;
    }

    @Override
    public Contest removeJurors(int contestId, User user, User logged) {
        Contest contest = contestRepository.getById(contestId);
        if (logged == contest.getOrganizer_id()) {
            throw new UnauthorizedOperationException("Only the organizer of the contest is allowed to remove jury.");
        }

        Set<User> jurors = contest.getJurors();
        jurors.remove(user);
        contestRepository.update(contest);

        return contest;
    }

    @Override
    public Contest addParticipants(int contestId, User user) {
        Contest contest = contestRepository.getById(contestId);

        Set<User> participants = contest.getParticipants();
        participants.add(user);
        contestRepository.update(contest);

        return contest;
    }


    /**
     * The method  updateFinishedContests() checks which contest should be in phase "Finished", given the current date and time,
     * but are still not marked as finished (getAllToBeMarkedAsFinished()). For each such contest, it adds lists of photos which have
     * top 3 score (calculateWinners()). The points of winning users and participants are calculated accordingly and added
     * to their total points (awardPoints()).
     **/
    @Override
    public void updateFinishedContests() {
        List<Integer> contestIds = contestPhaseRepository.getAllToBeMarkedAsFinished();
        for (Integer id :
                contestIds) {
            calculateWinners(id);
            awardPoints(id);
            Contest contest = getById(id);
            contest.setIs_finished(true);
            contestRepository.update(contest);
        }
    }

    @Override
    public List<User> getContestOrganizers() {
        return contestRepository.getContestOrganizers();
    }

    //Should be activated only when a contest is being marked finished!
    //TODO potentially rework based on hql+sql query returning PhotoSubmissions with first 3 distinct scores instead of getTop10
    public void calculateWinners(int contestId) {
        List<PhotoSubmission> candidates = photoSubmissionRepository.getTop10PhotoSubmissionsByContestID(contestId);
        Contest contest = getById(contestId);
        Set<PhotoSubmission> firstPlace = contest.getFirstPlaceWinners();
        Set<PhotoSubmission> secondPlace = contest.getSecondPlaceWinners();
        Set<PhotoSubmission> thirdPlace = contest.getThirdPlaceWinners();

        if (!firstPlace.isEmpty()) {
//            May be upgraded with error msg at a later stage
            return;
        }
        List<Long> top3Scores = candidates
                .stream()
                .map(e -> e.getTotalScore())
                .distinct()
                .limit(3)
                .collect(Collectors.toList());

        for (PhotoSubmission candidate :
                candidates) {
            if (candidate.getTotalScore() == top3Scores.get(0)) {
                firstPlace.add(candidate);
            } else if (candidate.getTotalScore() == top3Scores.get(1)) {
                secondPlace.add(candidate);
            } else if (candidate.getTotalScore() == top3Scores.get(2)) {
                thirdPlace.add(candidate);
            }
        }
        contestRepository.update(contest);
    }

    //    Should be activated right after the method calculateWinners!
    public void awardPoints(int contestId) {
        Contest contest = getById(contestId);
        awardPointsForParticipation(contest);
        if (contest.getFirstPlaceWinners().size() == 0) {
            return;
        }
        Set<PhotoSubmission> firstPlaceWinningPics = contest.getFirstPlaceWinners();
        awardPointsForXPlace(contest, 1, firstPlaceWinningPics);
        Set<PhotoSubmission> secondPlaceWinningPics = contest.getSecondPlaceWinners();
        awardPointsForXPlace(contest, 2, secondPlaceWinningPics);
        Set<PhotoSubmission> thirdPlaceWinningPics = contest.getThirdPlaceWinners();
        awardPointsForXPlace(contest, 3, thirdPlaceWinningPics);
        if (contest.getFirstPlaceWinners().size() == 1 && contest.getSecondPlaceWinners().size() > 0) {
            addBonusPointsIfNeeded(contest);
        }
    }

    public void awardPointsForParticipation(Contest contest) {
        for (User participant :
                contest.getParticipants()) {
            participant.setTotalPoints(participant.getTotalPoints() + 1);
            participant.setBadgeType();
            userRepository.update(participant);
        }
    }

    public void awardPointsForXPlace(Contest contest, int place, Set<PhotoSubmission> listOfWinningPics) {
        if (listOfWinningPics.size() > 1) {
            for (PhotoSubmission pic :
                    listOfWinningPics) {
                User sharedPlaceWinner = pic.getUser();
                sharedPlaceWinner.setTotalPoints(sharedPlaceWinner.getTotalPoints() + SHARED_PLACE_POINTS[place - 1]);
                sharedPlaceWinner.setBadgeType();
                userRepository.update(sharedPlaceWinner);
            }
        } else if (listOfWinningPics.size() == 1) {
            User onlyWinner = listOfWinningPics.stream().findFirst().get().getUser();
            onlyWinner.setTotalPoints(onlyWinner.getTotalPoints() + NORMAL_PLACE_POINTS[place - 1]);
            onlyWinner.setBadgeType();
            userRepository.update(onlyWinner);
        }
    }


    public void addBonusPointsIfNeeded(Contest contest) {
        double firstPlaceScore = (double) contest.getFirstPlaceWinners().stream().findFirst().get().getTotalScore();
        double secondPlaceScore = (double) contest.getFirstPlaceWinners().stream().findFirst().get().getTotalScore();
        User onlyWinner = contest.getFirstPlaceWinners().stream().findFirst().get().getUser();
        if (firstPlaceScore / secondPlaceScore >= 2) {
            onlyWinner.setTotalPoints(onlyWinner.getTotalPoints() + BONUS_FIRST_PLACE_POINTS_FOR_DOUBLE_SCORE);
            onlyWinner.setBadgeType();
            userRepository.update(onlyWinner);
        }
    }

    @Override
    public List getAllContestsDetailed(User user) {
        return contestRepository.getAllContestsDetailed(user);
    }

    //It doesn't throw exceptions if user not found. In that case, an empty list is returned.
    @Override
    public List getFinishedContestsByParticipant(int participantId){
        return contestRepository.getFinishedContestsByParticipant(participantId);
    }

    //It doesn't throw exceptions if user not found. In that case, an empty list is returned.
    @Override
    public List getRunningContestsByParticipant(int participantId){
        return contestRepository.getRunningContestsByParticipant(participantId);
    }

//    TODO rename
    @Override
    public List filterAllPostsCustom(Optional<String> contest_title, Optional<Integer> user_id, Optional<Integer> phase_id, Optional<String> sort, Optional<String> contests_judge, HttpSession httpSession) {
        return contestRepository.getAllContestsDetailedFilter(contest_title, user_id, phase_id, sort, contests_judge, httpSession);
    }

    @Override
    public boolean isParticipant(int contest_id, User user){
        Contest contest = contestRepository.getById(contest_id);
        if(contest.getParticipants().contains(user)){
            return true;
        }
        return false;
    }

    @Override
    public boolean isJury(int contest_id, User user){
        Contest contest = contestRepository.getById(contest_id);
        if(contest.getJurors().contains(user)){
            return true;
        }
        return false;
    }

    public boolean isParticipantJuryOrg(Contest contest, User user) {
        return  (isParticipant(contest.getContest_id(),user)
                || isJury(contest.getContest_id(),user))
                || (contest.getOrganizer_id().getUser_id() == user.getUser_id());
    }

    @Override
    public List<PhotoSubmission> getWinningPhotos(int contestId) {
        Contest contest = getById(contestId);
        Set<PhotoSubmission> firstPlaceWinningPics = contest.getFirstPlaceWinners();
        Set<PhotoSubmission> secondPlaceWinningPics = contest.getSecondPlaceWinners();
        Set<PhotoSubmission> thirdPlaceWinningPics = contest.getThirdPlaceWinners();
        List<PhotoSubmission> winners = Stream
                .of(firstPlaceWinningPics, secondPlaceWinningPics, thirdPlaceWinningPics)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        return winners;
    }

    @Override
    public List<Contest> getContestToJudge(User user){
        List<Contest> contests = new ArrayList<>();
        for (Contest contest: contestRepository.getAll()){
            if(isJury(contest.getContest_id(),user) && !contest.getIs_finished()){
                contests.add(contest);
            }
        }
        return contests;
    }



}


