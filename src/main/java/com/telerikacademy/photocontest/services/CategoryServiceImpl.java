package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.repositories.contracts.CategoryRepository;
import com.telerikacademy.photocontest.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<CategoryType> getAll() {
        return repository.getAll();
    }

    @Override
    public CategoryType getByID(int id) {
        return repository.getById(id);
    }
}
