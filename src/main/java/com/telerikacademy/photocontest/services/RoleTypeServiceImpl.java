package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.RoleType;
import com.telerikacademy.photocontest.repositories.contracts.RoleTypeRepository;
import com.telerikacademy.photocontest.services.contracts.RoleTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleTypeServiceImpl implements RoleTypeService {

    private final RoleTypeRepository roleTypeRepository;

    public RoleTypeServiceImpl(RoleTypeRepository roleTypeRepository) {
        this.roleTypeRepository = roleTypeRepository;
    }

    @Override
    public List<RoleType> getAll() {
        return roleTypeRepository.getAll();
    }
}
