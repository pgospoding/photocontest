package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.Score;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.telerikacademy.photocontest.repositories.contracts.ScoreRepository;
import com.telerikacademy.photocontest.services.contracts.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ScoreServiceImpl implements ScoreService {
    private final ScoreRepository repository;
    private final PhotoSubmissionRepository photoSubmissionRepository;

    @Autowired
    public ScoreServiceImpl(ScoreRepository repository, PhotoSubmissionRepository photoSubmissionRepository) {
        this.repository = repository;
        this.photoSubmissionRepository = photoSubmissionRepository;
    }

    @Override
    public List<Score> getAll() {
        return repository.getAll();
    }

    @Override
    public Score getById(int id) {
        return repository.getById(id);
    }

    public List<Score> getByPhotoId(int photo_id) {
        return repository.getListByField("photo_id", "photo_id");
    }

    @Override
    public Score create(Score score) {
        repository.create(score);
        PhotoSubmission photoSubmission = score.getPhotoSubmission();
        Set<Score> scores = photoSubmission.getScores();
        scores.add(score);
        if (scores.size() != 1) {
            photoSubmission.setTotalScore(photoSubmission.getTotalScore() + score.getScore());
        } else {
            photoSubmission.setTotalScore(score.getScore());
        }
        photoSubmissionRepository.update(photoSubmission);
        return score;
    }

    @Override
    public Score update(Score score, User user) {
        if (!score.getUser().getUsername().equals(user.getUsername()))
            throw new UnauthorizedOperationException("You are not the author of this item or an admin");
        repository.update(score);
        PhotoSubmission photoSubmission = score.getPhotoSubmission();
        Score scoreByUser = photoSubmission.getScores().stream().filter(s -> s.getUser().equals(user)).findFirst()
                .orElseThrow(ObjectNotFoundException::new);
        long remainingScore = photoSubmission.getTotalScore() - scoreByUser.getScore();
        long totalNewScore = remainingScore + score.getScore();
        scoreByUser.setScore(score.getScore());
        photoSubmission.setTotalScore(totalNewScore);
        photoSubmissionRepository.update(photoSubmission);
        return score;
    }

    @Override
    public String delete(int id, User user) {
        if (!repository.getById(id).getUser().getUsername().equals(user.getUsername()))
            throw new UnauthorizedOperationException("You are not the author of this item or an admin");
        PhotoSubmission photoSubmission = repository.getById(id).getPhotoSubmission();
        Score scoreByUser = photoSubmission.getScores().stream().filter(s -> s.getUser().equals(user)).findFirst()
                .orElseThrow(ObjectNotFoundException::new);
        long oldTotalScore = photoSubmission.getTotalScore();
        photoSubmission.setScores(photoSubmission.getScores().stream().filter(s -> !s.getUser().equals(user)).collect(Collectors.toSet()));
        if (photoSubmission.getScores().size() == 0) {
            photoSubmission.setTotalScore(3);
        } else {
            photoSubmission.setTotalScore(oldTotalScore - scoreByUser.getScore());
        }
        return repository.delete(id);
    }

    public void DeleteAllByContestID(int id) {
        repository.DeleteAllByContestID(id);
    }

    public Boolean getScoreByUserAndSubmission(int user_id, int submission_id) {
        return repository.getScoreByUserAndSubmission(user_id, submission_id);
    }
}
