package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestPhase;
import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.ContestPhaseRepository;
import com.telerikacademy.photocontest.repositories.contracts.ContestRepository;
import com.telerikacademy.photocontest.services.contracts.ContestPhaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContestPhaseServiceImpl implements ContestPhaseService {
    private final ContestPhaseRepository repository;
    private final ContestRepository contestRepository;

    @Autowired
    public ContestPhaseServiceImpl(ContestPhaseRepository repository, ContestRepository contestRepository) {
        this.repository = repository;
        this.contestRepository = contestRepository;
    }

    @Override
    public List<Contest> getAllByPhase(int phase_type_id) {
        return repository.getAllByPhase(phase_type_id);
    }

    @Override
    public List<ContestPhase> getAll() {
        return repository.getAll();
    }

    @Override
    public ContestPhase getById(int id) {
        return repository.getById(id);
    }


    @Override
    public void update(ContestPhase contestPhase, Contest contest, User loggedUser) {

        if (!contest.getOrganizer_id().equals(loggedUser) && !loggedUser.isActiveAdmin()) {
            throw new UnauthorizedOperationException("Only the organizer of the contest or an admin can modify a contest.");
        }

        repository.update(contestPhase);
    }

    @Override
    public void delete(int id, Contest contest, User loggedUser) {
        ContestPhase contestPhase = repository.getById(id);
        if (!contest.getOrganizer_id().equals(loggedUser) && !loggedUser.isActiveAdmin()) {
            throw new UnauthorizedOperationException("Only the organizer of the contest or an admin can modify a contest phase.");
        }
        repository.delete(id);
    }

    @Override
    public List<Integer> checkFinishedContests(){
        return repository.getAllToBeMarkedAsFinished();
    }

    @Override
    public void create(ContestPhase contestPhase) {
        repository.create(contestPhase);
    }

    @Override
    public ContestPhase getByID(int id){
        return repository.getById(id);
    }

    @Override
    public void deleteByContestID(int id, User loggedUser){
        Contest contest = contestRepository.getById(id);
        if (!contest.getOrganizer_id().equals(loggedUser) && !loggedUser.isActiveAdmin()) {
            throw new UnauthorizedOperationException("Only of the contest or an admin can modify a contest.");
        }
        List<ContestPhase> phasesToDelete = repository.getAllPhasesByContestID(contest.getContest_id());
        for(ContestPhase phase : phasesToDelete){
            repository.delete(phase);
        }
    }

    @Override
    public List<ContestPhase> getAllPhasesByContestID(int contestId){
        return repository.getAllPhasesByContestID(contestId);
    }

    @Override
    public ContestPhase getPhaseOneByContestID(int contestId){

        return repository.getPhaseByPhaseNumberAndContestID(1,contestId);
    }
    @Override
    public ContestPhase getPhaseTwoByContestID(int contestId){
        return repository.getPhaseByPhaseNumberAndContestID(2,contestId);

           }

    @Override
    public ContestPhase getPhaseThreeByContestID(int contestId){

        return repository.getPhaseByPhaseNumberAndContestID(3,contestId);    }

    @Override
    public List<PhaseType> getPhaseTypes() {
        return repository.getPhaseTypes();
    }

    @Override
    public Integer getContestPhase(int contestID) {
        return repository.getContestPhase(contestID);
    }

}
