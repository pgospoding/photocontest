package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.Status;
import com.telerikacademy.photocontest.repositories.contracts.StatusRepository;
import com.telerikacademy.photocontest.services.contracts.StatusService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getAll() {
        return statusRepository.getAll();
    }
}
