# Photocontest

A website for everyone to participate in photo contests and, at some point, create their own.

## Description
Functional Requirements

Public Part

The public part is accessible without authentication i.e. for anonymous users.

It contains landing page, options to log in and to register.

Login form  – redirects to the private area of the application. Login requires username and password.

Register form  – registers someone as a Regular User (potential participant). Requires username, email, first name, last name, and password.
Website admin can promote a regular user to Organizer or Admin.

Private part

Accessible only if the user is authenticated.

Dashboard page

Dashboard page  is different for Organizers and Regular users.

For Organizers: (any of the below can be either a new page, or directly on the dashboard).

o There must be a way to setup a new Contest.

o There must be a way to view Contests which are in Phase I.

o There must be a way to view Contests which are in Phase II.

o There must be a way to view Contests which are Finished.

o Must be able to make other users organizers.

o Must be able to delete contest entry (due to offensive nature of the photo, comment etc.) Jury must be able to delete as well.

o Jury must not be able to participate within the same contest where they are jury.

o There must be a way to view Photo Junkies.

· (If scoring is implemented) ordered by ranking 

For Photo Junkies:

o There must be a way to view active Open contests.

o There must be a way to view contests that the junkie currently participates in.

o There should be a way to view finished contests that the junkie participated in.

o Display current points and ranking and how much until next ranking at a visible place

Contest page

o The Contest Category is always visible.

o Phase I 

· Remaining time until Phase II should be displayed.

· Jury can view submitted photos but cannot rate them yet.

· Junkies see enroll button if the contest is Open and they are not participating.

· If they are participating and have not uploaded a photo, they see a form for upload:

§ Title – short text (required)

§ Story – long text, which tells the captivating story of the phot (required)

§ Photo – file (required)

· Only one photo can be uploaded per participant. The photo, title, and story cannot be edited. 

o Phase II 

· Remaining time until Finish phase.

· Participants cannot upload anymore.

· Jury sees a form for each submitted photo.

§ Score (1-10) (required)

§ Comment (long text) (required)

§ Checkbox to mark that the photo does not fit the contest category. If the checkbox is selected, score 0 is assigned automatically and a Comment that the category is wrong. This is the only way to assign Score outside the [1, 10] interval.

§ Each juror can give one review per photo, if a photo is not reviewed, a default score of 3 is awarded.

o Finished

· Jury can no longer review photos. 

· Participants view their score and comments. 

· In this phase, participants can also view the photos submitted by other users, along with their scores and comments by the Jury. (should)

Create Contest Page

Create Contest Form – either a new page, or on the organizer’s dashboard. The following must be easy to setup.

o Title – text field (required and unique)

o Category – text field (required)

o Open  or Invitational (should) Contest. Open means that everyone (except the jury can join)

· If invitational – a list of users should be available, along with the option to select them (should)

o Phase I time limit  – anything from one day to one month

o Phase II time limit  – anything from one hour to one day

o Select Jury – all users with Organizer role are automatically selected 

· Additionally, users with ranking Photo Master can also be selected, if the organizers decide 

o Cover photo (could) - a photo can be selected for a contest.

· Option 1 – upload a cover photo.

· Option 2 – paste the URL of an existing photo.

· Option 3 – select the cover from previously uploaded photos.

· The organizer must be able to choose between all three options and select the easiest for him/her (all 3 required if cover photo is implemented)

Scoring (should)

Contest participation should award points. Points are accumulative, so being invited and subsequently winning will award 53 points totals.

o Joining open contest – 1 point

o 3rd place – 20 points (10 points if shared 3rd)

o 2nd place – 35 points (25 points if shared 2nd)

o 1st place – 50 points (40 points if shared 1st)

o Finishing at 1st place with double the score of the 2nd (e.g., 1st has been awarded 8.6 points average, and 2nd is 4.3 or less) – 75 points

o In case of a tie, positions are shared, so there can be more than one participant at 1st, 2nd, and 3rd places, all in the same contest.

For example, two 1st places, one 2nd and four 3rds; the two winners will each get 40 points, the only 2nd place will get the full 35 points, and the four 3rd finishers will get 10 points.

Ranking:

o (0-50) points – Junkie

o (51 – 150) points – Enthusiast

o (151 – 1000) points – Master (can now be invited as jury)

o (1001 – infinity) points – Wise and Benevolent Photo Dictator (can still be jury)


REST API

To provide other developers with your service, you need to develop a REST API. It should leverage HTTP as a transport protocol and clear text JSON for the request and response payloads.

The REST API provides the following capabilities:

1. Users

· CRUD Operations 

· List and search by username, first name or last name 

2. Contests

· CRUD Operations 

· Submit photo 

· Rate photo 

· List and filter by title, category, type and phase 

3. Photos

· CRD Operations 

· List and search by title 


## Authors and acknowledgment
Alexandra Velikova, Georgi Krastev, Plamena Gospodinova

## Project status
Under development.
